## **B(FOM)^2 : block full orthogonalization methods for functions of matrices**

This is a versatile package implementing restarted block full orthogonalization methods (BFOM) for approximating the action of a matrix function on multiple vectors, i.e., $f(A)B$. The main purpose is to study the behavior of such methods for different parameter choices on different kinds of problems.  The papers cited below are introductions to a framework for the analysis of block Krylov subspace methods, in particular B(FOM)^2 and its modifications. Linear systems solvers are also included, as they inform the matrix function solvers and are useful for debugging and testing new features.

* [FrommerLundSzyld2017]: A. Frommer, K. Lund, and D. B. Szyld. Block Krylov subspace methods for functions of matrices, Electronic Transactions on Numerical Analysis, Vol. 47, pp. 100-126, 2017. https://etna.math.kent.edu/vol.47.2017/pp100-126.dir/pp100-126.pdf

* [FrommerLundSzyld2020]: A. Frommer, K. Lund, and D. B. Szyld. Block Krylov subspace methods for functions of matrices II: Modified block FOM, SIAM Journal on Matrix Analysis and Applications, 41(2), pp. 804--837, 2020. https://epubs.siam.org/doi/abs/10.1137/19M1255847

This package additionally runs tests used in

* [Lund2020]: K. Lund. The tensor t-function: a definition for functions of third-order tensors, Numerical Linear Algebra with Applications, 27 (3), e2288, 2020. https://onlinelibrary.wiley.com/doi/abs/10.1002/nla.2288

The tensor t-function is a generalization of matrix function definitions to third-order tensors via the t-product framework (see multiple sources within [Lund2020]).  The action of a t-function on a right-hand side can be recast as an $f(A)B$ problem, hence its inclusion in this package.

Throughout this file, the alphanumeric abbreviations in brackets will be used to refer to these publications.

## Installation
Clone this repository (`bfomfom`) and `chebfun` (https://github.com/chebfun/chebfun) into separate folders at the same level.  `chebfun` is not necessary for core functionality; it is, however, used in the quadrature rules for a couple functions (namely, `quad_invAlpha` and `quad_logShiftInv`).  So if you only care, e.g., about the matrix exponential, you can get by without `chebfun`.

### Getting your feet wet
See the `first_time_*.m` files in the `tests` subfolders for simple scripts demonstrating the variety of methods and their parameters.  For more details about each parameter, and some interesting performance issues, see `doc.pdf`.

## Reproducing figures in papers
Tests for each paper have been collected into separate folders.  We provide exact command-line instructions for ease of reproducibility.  Note that all .mat files have been moved to Zenodo, and the necessary .mat file for a given test usually follows the naming convention `test_name_exact_sol.mat`.  Despite the name, the .mat file contains the operator, right-hand side, exact solution, and sometimes some intermediate quantities.

### [FrommerLundSzyld2017]
* Fig. 5.1 -- `tridiag.m`
* Fig. 5.2, left -- `lapl_2d_case1.m`
* Fig. 5.2, right -- `lapl_2d_case2.m`
* Fig. 5.3 -- `qcd_dirac.m` -- tough matrices, and a parameter study -- best to run on a cluster
* Fig. 5.4 -- `power_bus.m` -- parameter study with timings -- best to run on a cluster
* Fig. 5.5, $\nu = 0, 100, 200$ -- `conv_diff.m` -- very large matrices -- best to run on a cluster.  Also note that while the plot for $\nu = 0$ is in the A-weighted Frobenius norm, the error for $\nu > 0$ is measured in just the Frobenius norm.  (So there's a typo in [FrommerLundSzyld2017].)

Note that .mat file necessary for `qcd_dirac.m` must be downloaded from [Zenodo](https://doi.org/10.5281/zenodo.6414224); the generating script has been lost.  (The script `qcd_dirac.m` will download the .mat file automatically.) It was later uncovered that the matrix Q is the same as or related to `conf6_0-8x8-30` in the [SuiteSparse collection](https://sparse.tamu.edu/QCD/conf6_0-8x8-30).  If you have issues downloading the QCD .mat file from Zenodo, it should be possible to reconstruct a close-enough problem from `conf6_0-8x8-30` for reproducing the behavior observed in the paper.  See `qcd_nonzero_chempotential_exact_sol.m` in [FrommerLundSzyld2020].

### [FrommerLundSzyld2020]
Nearly all figures in this paper are the result of resource-intensive[^1] tests.  We highly recommend running them on a cluster.  See the folder `shell_scripts` for bash script templates.  Note also that the test files written for this paper are quite versatile, in that they take multiple parameters and allow for comparisons across different matrices, block inner products, and modifications.  The three key parameters with all possible options are as follows:
* `caseID = 1:8`, where each number corresponds to a diagonal matrix with a specified eigenvalue distribution.  See `data\lin_sys_mod_setup.m` for details.  Only matrices `1`, `2`, and `4` (Examples 5.1(a), (b), and (c), respectively) are considered in the paper.
* `modID = 1:3`, with `1`: `none`, `2`:`harmonic`, `3`:`radau-arnoldi` denoting different low-rank modifications to the Hessenberg matrix.
* `ipID = 1:5`, with `1`:`cl_full`, `2`:`gl`, `3`:`li`, `4`:`nb`, `5`:`hy` denoting different block inner products.  Only `1:3` are considered in this paper, due to conclusions at the end of [FrommerLundSzyld2017].

There are obvious pros and cons to this modular approach; we anyway do our best to provide the exact parameters to reproduce figures in the paper.
* Fig. 4.1 -- `residual_bound_violation.m` -- relatively fast
* Fig. 5.1 -- `lin_sys_mod_compare(1:2, 1:3, 1:3)`
* Fig. 5.2 -- `lin_sys_mod('conv_plots', 1:2, 1:3, 1:3)` and `lin_sys_mod('cycle_plots_vary_m', 1:2, 1:3, 1:3)`
* Fig. 5.3 -- `lin_sys_mod('conv_plots', 4 , 1:3, 1:3)`
* Fig. 5.4 -- `invSqrt_mod([1 4], 1:3, 1:3)` and `invSqrt_mod_plots([1 4], 1:3, 1:3)`
* Fig. 5.5 -- `logShiftInv_mod(4, 1:3, 1:3)`
* Tab. 5.1 -- `qcd_nonzero_chempotential_mod(3, 1:3, [2 5 10], 1:3, 'large')` -- very resource-intensive!  Try `system_size = 'small'` if you want to see what happens locally (but it will still take a while).  Run `max_cycles = qcd_nonzero_chempotential_counts(3,'large')` afterwards to extract values for the table.

The following tests were excluded from the paper, but may still be of interest (particularly for comparing to related methods).
* `lapl_2d_mod.m` -- compares with non-modified methods from [FrommerLundSzyld2017]; uses same matrices generated by `lapl_2d_exact_sol.m` in `tests\FrommerLundSzyld2017\data`.
* `conv_diff_mod.m` -- compares with non-modified methods from [FrommerLundSzyld2017]; uses same matrices generated by `conv_diff_exact_sol.m` in `tests\FrommerLundSzyld2017\data`.
* `nonnorm_nondiag.m` -- emulates results from column-wise f(A)b approaches in [FrommerGuettelSchweitzer2014][^2]
* `lin_sys_mod('cycle_plots_vary_s', caseID, modID, ipID)` -- varies the number of right-hand sides `s` and reports the number of cycles needed to converge per `s`.  The default range for `s` is `5:5:20`.

Lastly, a feature was developed for looking at the norm of the residual polynomial of the first cycle of a given method.  Run something like `lin_sys_mod('poly_plots', 8, 1:2, 1:3)` and try to make sense of what happens. ;)

### [Lund2020]
The tensor t-function examples are quite lightweight compared to those from the previous two papers.  There is essentially one test considered.  The data matrices can easily be generated locally, and the tests are relatively quick.
* Figure 2 -- `data\tensor_texp_network_v2_exact_sol.m`
* Figure 4 -- `tensor_texp_network_v2(5)`
* Figure 5 -- `tensor_texp_network_v2(2)`
* Table 1 -- the data from Figures 4 and 5, plus additional runs of `tensor_texp_network_v2` with `m = 10` and `m = 15`

## A word of caution
Few scripts are optimized for performance and even then may require a significant amount of memory and time to run. If you want to run any of the large-scale parameter studies, we recommend using a cluster.  The shell scripts used to generated plots in [FrommerLundSzyld2020] are included and may be helpful templates for your own cluster.

Note also that outputs (e.g., .mat and .fig files) are saved to a `results` folder. Some tests generate and save large amounts of data and figures.  Just comment out the right sections of code to avoid saving big files.

## Links to .mat files for each paper
Some tests require large .mat matrices to run.  If you have trouble running the data-generator scripts (usually called `*_exact_sol.m`) or if the script is missing (as in the case of `qcd_dirac.m`), then head on over to Zenodo to download the pre-generated .mat files directly.  Even relatively fast tests (e.g., `lapl_2d_case1` and `lapl_2d_case2`) may take a while to generate data matrices, due to dense linear algebra routines being used to estimate highly accurate solutions.
* FrommerLundSzyld2017: https://doi.org/10.5281/zenodo.6414224
* FrommerLundSzyld2020: https://doi.org/10.5281/zenodo.6417952
* Lund2020: https://doi.org/10.5281/zenodo.6420778

Otherwise, .mat files will be generated and saved on your local machine the first time you run a script in `tests`.

## "Why didn't you just leave the .mat files in this repo?"
I did at first, but this led to a bloated repo on the order of 2GB.  I have since purged all these files, as well as their histories (via `git filter-repo`).

## Questions, complaints, etc.
Please contact me directly: kathryn.d.lund (at) gmail (dot) com.  I'm happy to provide access to my papers and code when paywalls and firewalls make things difficult.  Maintaining this chimera of code is not easy, but I'm happy to make updates when things break.  The most recent changes were made in Matlab 2022a.

## Related work
Much of the syntax for this project was originally inspired by or copied from [`funm_quad`](https://www.guettel.com/funm_quad/). If you're interested in blocking algorithms in general, you might also like [`BlockStab`](https://github.com/katlund/BlockStab) and [`LowSyncBlockArnoldi`](https://gitlab.mpi-magdeburg.mpg.de/lund/low-sync-block-arnoldi).

## Acknowledgements
Many thanks [Teodor Nikolov](https://github.com/teonnik) for general git support, as well as [Nils-Arne Dreier](https://gitlab.dune-project.org/nils.dreier) and [Liam Burke](https://www.maths.tcd.ie/people/burke/) for testing and reporting bugs.











[^1]: and, let's be real, probably inefficiently written
[^2]: A. Frommer, S. Güttel, and M. Schweitzer. Convergence of restarted Krylov subspace methods for Stieltjes functions of matrices, SIAM Journal on matrix Analysis and Applications, Vol. 35, 4, pp. 1602-1624, 2014.
