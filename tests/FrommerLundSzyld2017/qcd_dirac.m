% QCD_DIRAC runs bfomfom and fomfom for the QCD Dirac overlap operator
% example.
%
% This file has been adapted from funm_quad
% (http://eprints.maths.manchester.ac.uk/2266/), the body of code
% associated to [A. Frommer, S. Guettel, and M. Schweitzer: Efficient and
% stable Arnoldi restarts for matrix functions based on quadrature, SIAM J.
% Matrix Anal. Appl., 35:661-683, 2014].
%
% This file generates Fig. 5.3 in in [FrommerLundSzyld2017].
%
% Unfortunately, the file for generating qcd_dirac_exact_sol.mat has been
% lost.  Please visit [https://doi.org/10.5281/zenodo.6414224] to download
% the .mat file.
%
% It was later uncovered that the matrix Q is the same as or related to
% 'conf6_0-8x8-30' in the SuiteSparse collection
% (https://sparse.tamu.edu/QCD/conf6_0-8x8-30).  If you have issues
% downloading the .mat file from Zenodo, it should be possible to
% reconstruct a close-enough problem from 'conf6_0-8x8-30' for reproducing
% the behavior observed in [FrommerLundSzyld2017].
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))
out.teststr = mfilename;

tol_err = 1e-6;
verbose = 1;
loadstr = 'data/qcd_dirac_exact_sol.mat';

if isfile(loadstr)
    data = load(loadstr,'Q2','QB','exact_sol');
else
    fprintf('File %s is missing,\nand it cannot be generated from a script.\n', loadstr);
    fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
    data = load(websave(loadstr, 'https://zenodo.org/record/6414224/files/qcd_dirac_exact_sol.mat?download=1'),...
        'Q2','QB','exact_sol');
end

[n, s] = size(data.QB(:,1:12));
q = 4;
% Note that we are actually computing sign(Q)B = sqrt(Q^2)\(Q*B).  The
% matrix Q2 = Q^2, and the RHS here to be considered is QB.  Q and B were
% the original data, however.

% Un-comment the following to generate sparsity figure, but beware: it is
% resource-intensive!
% spy(A);
% savestr = sprintf('%s_sparsity_pattern',teststr);
% savefig(gcf,savestr);

out.inner_product = {'cl_full', 'gl', 'li', 'nb','hy'};
lIP = length(out.inner_product);
out.inner_product_legend = {'Cl', 'Gl', 'Li', 'nB','Hy'};
out.inner_product_plot = {'-','--','-.',':','o'};
out.cmap = hsv(lIP)*.85;

m = [25 50 100 150];
lm = length(m);

out.data = zeros(lIP,6);
out.exact_error = cell(lIP,lm);
out.times = cell(lIP,lm);

for j = 1:lm
    out.savestr = sprintf('results/%s/case_n%g_s%g_m%g',out.teststr,n,s,m(j));
    for c = 1:lIP
        param = [];
        param.exact = data.exact_sol(:,1:12);
        param.norm = 'A-fro';                                               % specify norm error should be measured in
        param.error_scale = block_norm(param.exact, param, data.Q2);        % compute norm of error for scaling
        param.function = 'invSqrt';
        param.hermitian = true;
        param.inner_product = out.inner_product{c};
        param.restart_length = m(j);
        param.tol_err = tol_err;
        param.tol_quad = tol_err;
        param.verbose = verbose;
        param = param_init_bfomfom(param);
        param.q = q;
        param = bfomfom(data.Q2, data.QB(:,1:12), param); fprintf('\n');

        out.times{c} = param.time;
        out.exact_error{c} = param.exact_error;
        out.data(c,1) = sum(out.times{c});                                  % store total time
        out.data(c,2) = param.Acount;                                       % store number of average A products per cycle
        out.data(c,3) = length(out.exact_error{c});                         % store number of cycles
        out.data(c,4) = out.data(c,1)/out.data(c,3);                        % store average time
        out.data(c,5) = out.exact_error{c}(end);                            % store final true error
        out.data(c,6) = param.approx_error(end);                            % store final approximate error
    end
    out.norm = param.norm;
    out = format_test_output(out);
end

%% Additional plots
% Written so that plots can be generated without having to re-run the
% entire test
addpath(sprintf('%s',out.teststr))
total_time = zeros(lIP,lm);
cycle_count = zeros(lIP,lm);

for j = 1:lm
    load(sprintf('%s_n%g_s%g_m%g',out.teststr,n,s,m(j)))
    total_time(:,j) = out.data(:,1);
    cycle_count(:,j) = out.data(:,3);
end

% total time vs. cycle length
figure
for c = 1:lIP
    if c < 5
        plot(m,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineWidth',2,...
            'LineStyle',out.inner_product_plot{c});
    else
        plot(m,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineStyle','-',...
            'LineWidth',1,...
            'MarkerSize',2,...
            'Marker',out.inner_product_plot{c});
    end
    if c == 1
        hold on
    end
end
xlabel('m (cycle length)')
ylabel('wall time (s)')
legend(out.inner_product_legend,'Location','Best')
grid on
hold off
savestr = sprintf('%s/%s_time',out.teststr,out.teststr);
savefig(gcf,savestr);


% number of cycles vs. cycle length
figure
for c = 1:lIP
    if c < 5
        plot(m,cycle_count(c,:),...
            'Color',out.cmap(c,:),...
            'LineWidth',2,...
            'LineStyle',out.inner_product_plot{c});
    else
        plot(m,cycle_count(c,:),...
            'Color',out.cmap(c,:),...
            'LineStyle','-',...
            'LineWidth',1,...
            'MarkerSize',2,...
            'Marker',out.inner_product_plot{c});
    end
    if c == 1
        hold on
    end
end
xlabel('m (cycle length)')
ylabel('number of cycles')
legend(out.inner_product_legend,'Location','Best')
set(gca,'FontSize',16);
grid on
hold off
savestr = sprintf('%s/%s_cycles',out.teststr,out.teststr);
savefig(gcf,savestr);
