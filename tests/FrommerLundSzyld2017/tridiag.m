% TRIDIAG runs bfomfom and fomfom for an HPD tridiagonal matrix to estimate
% the solution to A^{-1/2}B.
%
% This file generates Fig. 5.1 in [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))
out.teststr = mfilename;

tol_err = 1e-10;                                                            % desired accuracy
verbose = 1;                                                                % whether or not to produce intermediate outputs

loadstr = 'data/tridiag_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        tridiag_exact_sol;
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6414224/files/tridiag_exact_sol.mat?download=1'));
    end
end

A = data.A;
B = data.B;
exact_sol = data.exact_sol;
eigA = data.eigA;
clear data     % free up space
[n,s] = size(B);
q = 5;                                                                      % block size for hybrid method
m = 5;                                                                      % cycle length

out.savestr = sprintf('results/%s/case_n%g_s%g_m%g',out.teststr,n,s,m);     % string for naming files
out.condA = cond(A);                                                        % condition number of A

out.inner_product = {'cl_full', 'gl', 'li', 'nb','hy'};                     % which inner products to run
lIP = length(out.inner_product);
out.inner_product_legend = {'Cl', 'Gl', 'Li', 'nB','Hy','bound'};
out.inner_product_plot = {'-','--','-.',':','o'};
out.cmap = hsv(lIP)*.85;

out.data = zeros(lIP,6);                                                    % storage for outputs
out.exact_error = cell(lIP,1);
out.times = cell(lIP,1);

for c = 1:5
    param = [];                                                             % clear and initialize 'param'
    param.exact = exact_sol;                                                % assign exact solution, if known
    param.norm = 'A-fro';                                                   % specify norm error should be measured in
    param.error_scale = block_norm(param.exact,param,A);                    % compute norm of error for scaling
    param.function = 'invSqrt';                                             % specify function
    param.hermitian = true;                                                 % whether A is Hermitian or not (default is not)
    param.inner_product = out.inner_product{c};
    param.restart_length = m;
    param.tol_err = tol_err;
    param.tol_quad = tol_err;                                               % specify quadrature tolerance
    param.verbose = verbose;
    param = param_init_bfomfom(param);                                      % initialize all other parameter fields
    param.q = 5;                                                            % size of hybrid blocks
    param = bfomfom(A,B,param); fprintf('\n');
    
    out.times{c} = param.time;
    out.exact_error{c} = param.exact_error;
    
    out.data(c,1) = sum(out.times{c});                                      % store total time
    out.data(c,2) = param.Acount;                                           % store number of average A products per cycle
    out.data(c,3) = length(out.exact_error{c});                             % store number of cycles
    out.data(c,4) = out.data(c,1)/out.data(c,3);                            % store average time
    out.data(c,5) = out.exact_error{c}(end);                                % store final true error
    out.data(c,6) = param.approx_error(end);                                % store final approximate error
end
out.norm = param.norm;
out = format_test_output(out);

%% Plotting error bound
lmax = max(eigA); lmin = min(eigA);
gamma = norm(B,'fro')*sqrt(lmax)/sqrt(sqrt(lmax*lmin))/sqrt(lmin);          % division by sqrt(lmin) to account for A-weight norm
kappa = lmax/lmin;
c = (kappa-1)/(kappa+1);
xi_m = 1/cosh(m*log(c));
bound = @(k) gamma*xi_m.^k/param.error_scale;
max_restarts = max(out.data(:,3));

hold on
semilogy(bound(1:max_restarts),'k','LineWidth',2);                          % plot theoretical convergence bound
legend(out.inner_product_legend,'Location','Best')
hold off
