% FIRST_TIME_MAT_FUNC runs bfomfom for a small problem to estimate the
% solution to exp(A)*B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))

n = 100; s = 4;
A = -spdiags((1:n)',0,n,n);
B = randn(n,s);

ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb', 'hy'};

param = [];
param.exact = expm(full(A))*B;                                              % assign exact solution, if known
param.function = 'exp';
param.error_scale = norm(param.exact, 'fro');                               % scale error

param.verbose = 0;

for ip = 1:5
    param.inner_product = ip_options{ip};                                   % choice of block inner product
    param.q = 2;                                                            % specify block size for hybrid inner product
    param.restart_length = 5;                                               % number of basis vectors before restart
    param = param_init_bfomfom(param);                                      % initialize all other parameter fields
    param = bfomfom(A,B,param);
    fprintf('%s: param.Fm approximates expm(A)B with accuracy %g in %d cycles.\n\n',...
        ip_str{ip},param.exact_error(end),length(param.exact_error))
end