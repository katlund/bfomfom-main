% POWER_NETWORK runs bfomfom and fomfom for the power network example.
%
% This file generates Fig. 5.4 in in [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))
out.teststr = mfilename;

tol_err = 1e-6;
verbose = 1;

loadstr = 'data/power_network_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        power_network_exact_sol;
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        % Made a mistake and uploaded the file with the wrong name...
        data = load(websave(loadstr, 'https://zenodo.org/record/6414224/files/power_bus_exact_sol.mat?download=1'));
    end
end
A = data.A;
exact_sol = data.exact_sol;
B = data.B;
clear data

n = size(A,1);
m = 50;
spy(A);
savestr = sprintf('results/%s/sparsity_pattern',out.teststr);
dirstr = sprintf('results/%s',out.teststr);
mkdir(dirstr)
savefig(gcf,savestr);

out.inner_product = {'cl_full', 'gl', 'li', 'nb','hy'};
lIP = length(out.inner_product);
out.inner_product_legend = {'Cl', 'Gl', 'Li', 'nB','Hy'};
out.inner_product_plot = {'-','--','-.',':','o'};
out.cmap = hsv(lIP)*.85;

s = [2 6 12 24];
q = [1 2 4 6];
lens = length(s);

out.data = zeros(lIP,6);
out.exact_error = cell(lIP,lens);
out.times = cell(lIP,lens);

for j = 1:lens
    fprintf('s = %g\n',s(j));
    param = [];
    out.savestr = sprintf('results/%s/case_n%g_s%g_m%g',out.teststr,n,s(j),m);
    for c = 1:lIP
        param = [];
        param.exact = exact_sol{j};
        param.norm = 'A-fro';                                               % specify norm error should be measured in
        param.error_scale = block_norm(param.exact,param,A);                % compute norm of error for scaling
        param.function = 'logShiftInv';
        param.hermitian = true;
        param.inner_product = out.inner_product{c};
        param.halt_if_diverge = false;
        param.restart_length = m;
        param.tol_err = tol_err;
        param.tol_quad = tol_err;
        param.verbose = verbose;
        param = param_init_bfomfom(param);
        param.q = q(j);
        param = bfomfom(A,B{j},param);  fprintf('\n');
        
        out.times{c} = param.time;
        out.exact_error{c} = param.exact_error;
        out.data(c,1) = sum(out.times{c});                                  % store total time
        out.data(c,2) = param.Acount;                                       % store number of average A products per cycle
        out.data(c,3) = length(out.exact_error{c});                         % store number of cycles
        out.data(c,4) = out.data(c,1)/out.data(c,3);                        % store average time
        out.data(c,5) = out.exact_error{c}(end);                            % store final true error
        out.data(c,6) = param.approx_error(end);                            % store final approximate error
    end
    out.norm = param.norm;
    out = format_test_output(out);
    save(sprintf(out.savestr),'out');
end

%% Additional plots
% Written so that plots can be generated without having to re-run the
% entire test
total_time = zeros(lIP,lens);
cycle_count = zeros(lIP,lens);

for j = 1:lens
    load(sprintf('results/%s/case_n%g_s%g_m%g',out.teststr,n,s(j),m))
    total_time(:,j) = out.data(:,1);
    cycle_count(:,j) = out.data(:,3);
end

% total time vs. cycle length
figure
for c = 1:lIP
    if c < 5
        plot(s,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineWidth',2,...
            'LineStyle',out.inner_product_plot{c});
    else
        plot(s,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineStyle','-',...
            'LineWidth',1,...
            'MarkerSize',2,...
            'Marker',out.inner_product_plot{c});
    end
    if c == 1
        hold on
    end
end
xlabel('s (number of columns)')
ylabel('wall time (s)')
legend(out.inner_product_legend,'Location','Best')
grid on
hold off
savestr = sprintf('results/%s/%s_time',out.teststr,out.teststr);
savefig(gcf,savestr);


% number of cycles vs. cycle length
figure
for c = 1:lIP
    if c < 5
        plot(s,cycle_count(c,:),...
            'Color',out.cmap(c,:),...
            'LineWidth',2,...
            'LineStyle',out.inner_product_plot{c});
    else
        plot(s,cycle_count(c,:),...
            'Color',out.cmap(c,:),...
            'LineStyle','-',...
            'LineWidth',1,...
            'MarkerSize',2,...
            'Marker',out.inner_product_plot{c});
    end
    if c == 1
        hold on
    end
end
xlabel('s (number of columns)')
ylabel('number of cycles')
legend(out.inner_product_legend,'Location','Best')
grid on
hold off
savestr = sprintf('results/%s/%s_cycles',out.teststr,out.teststr);
savefig(gcf,savestr);
