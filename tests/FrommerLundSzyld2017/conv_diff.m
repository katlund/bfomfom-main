% CONV_DIFF runs bfomfom and fomfom for the convection diffusion examples.
%
% This file has been adapted from funm_quad, the body of code associated to
% [A. Frommer, S. Guettel, and M. Schweitzer: Efficient and stable Arnoldi
% restarts for matrix functions based on quadrature, SIAM J. Matrix Anal.
% Appl., 35:661-683, 2014].
%
% This file generates Fig. 5.5 in [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))
out.teststr = mfilename;

tol_err = 1e-6;
verbose = 1;

m = 50;
nu = [0 100 200];
lnu = length(nu);

out.inner_product = {'cl_full', 'gl', 'li', 'nb', 'hy'};
lIP = length(out.inner_product);
out.inner_product_legend = {'Cl', 'Gl', 'Li', 'nB', 'Hy'};
out.inner_product_plot = {'-','--','-.',':','o'};
out.cmap = hsv(lIP)*.85;

out.data = zeros(lIP,6);
out.exact_error = cell(lIP,lnu);
out.times = cell(lIP,lnu);

for j = 1:lnu
%     profile on
    loadstr = sprintf('data/conv_diff%g_exact_sol.mat',nu(j));
    if isfile(loadstr)
        data = load(loadstr);
    else
        inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
        decision = input(inputstr);
        if decision == 1
            fprintf('The missing .mat file will be generated locally.\n');
            conv_diff_exact_sol;
            data = load(loadstr);
        elseif decision == 0
            fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
            downloadstr = sprintf('https://zenodo.org/record/6414224/files/conv_diff%d_exact_sol.mat?download=1',nu(j));
            data = load(websave(loadstr, downloadstr));
        end
    end
    switch nu(j)
        case 0
            A = data.A0;
            hermitian = true;
            exact_sol = data.exact_sol0;
            
        case 100
            A = data.A100;
            hermitian = false;
            exact_sol = data.exact_sol100;
            
        case 200
            A = data.A200;
            hermitian = false;
            exact_sol = data.exact_sol200;
    end
    B = data.B;
    [n,s] = size(B);
    q = 5;
    clear data
    
    out.savestr = sprintf('results/%s/case%g_n%g_s%g_m%g',out.teststr,nu(j),n,s,m);      % string for naming files
    for c = 1:lIP
        param = [];
        param.exact = exact_sol;
        param.function = 'exp';
        param.hermitian = hermitian;
        param.norm = 'fro';
        param.error_scale = block_norm(param.exact,param);                  % compute norm of error for scaling
        % Note: there are a couple errors in Fig 5.5.  The plot with nu = 0
        % is measured in the A-weighed Frobenius norm, while the other two
        % are measured in just the Frobenius norm, but labeled incorrectly.
        % Weighting with A is not possible for nu > 0.
        param.inner_product = out.inner_product{c};
        param.halt_if_diverge = false;
        param.restart_length = m;
        param.tol_err = tol_err;
        param.tol_quad = tol_err;
        param.verbose = 0;
        param = param_init_bfomfom(param);
        param.q = q;
        param = bfomfom(A,B,param);

        out.times{c} = param.time;
        out.exact_error{c} = param.exact_error;
        out.data(c,1) = sum(out.times{c});                                  % store total time
        out.data(c,2) = param.Acount;                                       % store number of average A products per cycle
        out.data(c,3) = length(out.exact_error{c});                         % store number of cycles
        out.data(c,4) = out.data(c,1)/out.data(c,3);                        % store average time
        out.data(c,5) = out.exact_error{c}(end);                            % store final true error
        out.data(c,6) = param.approx_error(end);                            % store final approximate error
    end
    out.norm = param.norm;
    out = format_test_output(out);
    save(sprintf(out.savestr),'out');
%     profile off
%     profstr = sprintf('%s/%s_nu%g',out.teststr,out.teststr,nu(j));
%     profsave(profile('info'),profstr)
end

%% Additional plots
% Written so that plots can be generated without having to re-run the
% entire test
n = 350^2; s = 10;
total_time = zeros(lIP,lnu);
addpath(sprintf('%s',out.teststr))

for j = 1:lnu
    load(sprintf('results/%s/case%g_n%g_s%g_m%g',out.teststr,nu(j),n,s,m))
    total_time(:,j) = out.data(:,1);
end

% total time vs. nu
figure
for c = 1:lIP
    if c < 5
        plot(nu,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineWidth',2,...
            'LineStyle',out.inner_product_plot{c});
    else
        plot(nu,total_time(c,:),...
            'Color',out.cmap(c,:),...
            'LineStyle','-',...
            'LineWidth',1,...
            'MarkerSize',2,...
            'Marker',out.inner_product_plot{c});
    end
    if c == 1
        hold on
    end
end
xlabel('{\nu}')
ylabel('wall time (s)')
legend(out.inner_product_legend,'Location','Best')
grid on
hold off
savestr = sprintf('results/%s/time',out.teststr,out.teststr);
savefig(gcf,savestr);
