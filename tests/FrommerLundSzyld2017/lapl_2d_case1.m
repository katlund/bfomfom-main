% LAPL_2D_CASE1 runs bfomfom and fomfom for the first Laplacian example.
%
% This file generates the left part of Fig. 5.2 in [FrommerLundSzyld2017].

%%
addpath(genpath('../../'))
% case 1: B has full rank
out.teststr = mfilename;

tol_err = 1e-6;
verbose = 1;

loadstr = 'data/lapl_2d_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr,'A','B1','exact_sol2');
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        lapl_2d_exact_sol
        data = load(loadstr,'A','B1','exact_sol2');
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6414224/files/lapl_2d_exact_sol.mat?download=1'));
    end
end

A = data.A;
B = data.B1;
exact_sol = data.exact_sol1;
clear data  % free up space

[n, s] = size(B);
m = 25;

out.savestr = sprintf('results/%s/case_n%g_s%g_m%g',out.teststr,n,s,m);     % string for naming files
% out.condA = condest(A);                                                   % condition number of A

out.inner_product = {'cl_full', 'gl', 'li', 'nb', 'cl_defl'};
lIP = length(out.inner_product);
out.inner_product_legend = {'Cl', 'Gl', 'Li', 'nB', 'Cl with deflation'};
out.inner_product_plot = {'-','--','-.',':','x'};
out.cmap = hsv(lIP)*.85;

out.data = zeros(lIP,6);
out.exact_error = cell(lIP,1);
out.times = cell(lIP,1);

for c = 1:lIP
    param = [];
    param.exact = exact_sol;
    param.norm = 'A-fro';                                                   % specify norm error should be measured in
    param.error_scale = block_norm(param.exact,param,A);                    % compute norm of error for scaling
    param.function = 'invSqrt';
    param.hermitian = true;
    param.inner_product = out.inner_product{c};
    param.restart_length = m;
    param.tol_err = tol_err;
    param.tol_quad = tol_err;
    param.verbose = verbose;
    param = param_init_bfomfom(param);
    param = bfomfom(A,B,param);  fprintf('\n');
    
    out.times{c} = param.time;
    out.exact_error{c} = param.exact_error;
    out.data(c,1) = sum(out.times{c});                                      % store total time
    out.data(c,2) = param.Acount;                                           % store number of average A products per cycle
    out.data(c,3) = length(out.exact_error{c});                             % store number of cycles
    out.data(c,4) = out.data(c,1)/out.data(c,3);                            % store average time
    out.data(c,5) = out.exact_error{c}(end);                                % store final true error
    out.data(c,6) = param.approx_error(end);                                % store final approximate error
end
out.norm = param.norm;
out = format_test_output(out);
