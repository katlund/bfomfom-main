% POWER_NETWORK_EXACT_SOL generates and saves "exact" (highly accurate)
% solutions for the power network example.  The matrix 1138_bus is taken
% from the SuiteSparse Matrix Collection:
% https://sparse.tamu.edu/HB/1138_bus
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017].

%%
addpath(genpath('../../../'))

load(websave('data/tmp', 'https://suitesparse-collection-website.herokuapp.com/mat/HB/1138_bus.mat'))

A = Problem.A;
clear Problem
n = size(A,1);
logShiftA = logm(full(A) + eye(n));

B = cell(1,5);
exact_sol = B;
s = [2 6 12 24 48];
for j = 1:length(s)
    B{j} = rand(n,s(j));
    exact_sol{j} = logShiftA*(A\B{j});
end

savestr = sprintf('data/%s',mfilename);
save(savestr);