% TRIDIAG_EXACT_SOL generates a Hermitian positive definite tridiagonal
% matrix, a random block vector B, and the solution A^{-1/2}B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017].

%%
addpath(genpath('../../../'))

n = 100; s = 10;
d0 = 10*randi(n,n,1);
d1 = -ones(n,1);
A = spdiags([d1 d0 d1],-1:1,n,n);
B = rand(n,s);
exact_sol = sqrtm(full(A))\B;
eigA = eig(full(A));

savestr = sprintf('data/%s',mfilename);
save(savestr);