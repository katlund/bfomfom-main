% LAPL_2D_EXACT_SOL generates and saves "exact" (highly accurate) solutions
% for the Laplacian examples.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
addpath(genpath('../../../'))

n = 100;
s = 10;
A = (n+1)^2*gallery('poisson',n);
ev = eigs(A,1,'SM');
A = A/ev;
fullA = full(A);
n = n^2;

%% Case 1: base problem
e = ones(n/s,1);
Is = eye(s);
B1 = kron(e,Is);

exact_sol1 = sqrtm(fullA)\B1;

%% Case 2: b_1 is a linear combination of A*b_2 through A*b_5
B2 = B1;
c = (2:5)';
B2(:,1) = A*B2(:,2:5)*c;  % forced linear dependence

exact_sol2 = sqrtm(fullA)\B2;

%%
save(mfilename)