% CONV_DIFF_EXACT_SOL generates and saves "exact" (highly accurate)
% solutions for the convection-diffusion example.  This example is
% especially memory-intensive; one should check system requirements before
% running.
%
% This file has been adapted from funm_quad, the body of code associated to
% [A. Frommer, S. Guettel, and M. Schweitzer: Efficient and stable Arnoldi
% restarts for matrix functions based on quadrature, SIAM J. Matrix Anal.
% Appl., 35:661-683, 2014].
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
addpath(genpath('../../../'))
N = 350;                                                                    % build discretization matrix for 2D convection-diffusion problem
D2 = (N+1)^2*gallery('tridiag',N);
I = speye(N);
D2 = kron(I,D2) + kron(D2,I);
o = ones(N,1);
D1 = (N+1)/2*spdiags([-o,0*o,o],-1:1,N,N);
D1 = kron(I,D1) + kron(D1,I);
dt = 2*1e-3;                                                                % choose time step dt = 2*1e-3

n = N^2;
s = 10;
e = ones(n/s,1);                                                            % same RHS as for Laplacian
Is = eye(s);
B = kron(e,Is);

param.error_scale = 1;                                                      % since we assume we don't know the exact error
param.function = 'exp';
param.halt_if_diverge = false;
param.tol_err = 5e-12;
param.tol_quad = 5e-10;                                                     % the default of 5e-16 is too stringent; nothing converges
param.verbose = 0;
param.m = 1000;
param = param_init_bfomfom(param);

nu = 0;
A0 = -dt*(D2 + nu*D1);
param.hermitian = true;
param = fomfom(A0,B,param);
exact_sol0 = param.Fm;
savestr = sprintf('conv_diff%g_exact_sol',nu);
save(savestr,'nu','A0','exact_sol0','B','N')

clear all %#ok<*CLALL> 

N = 350;                                                                    % build discretization matrix for 2D convection-diffusion problem
D2 = (N+1)^2*gallery('tridiag',N);
I = speye(N);
D2 = kron(I,D2) + kron(D2,I);
o = ones(N,1);
D1 = (N+1)/2*spdiags([-o,0*o,o],-1:1,N,N);
D1 = kron(I,D1) + kron(D1,I);
dt = 2*1e-3;                                                                % choose time step dt = 2*1e-3

n = N^2;
s = 10;
e = ones(n/s,1);                                                            % same RHS as for Laplacian
Is = eye(s);
B = kron(e,Is);

param.error_scale = 1;                                                      % since we assume we don't know the exact error
param.function = 'exp';
param.halt_if_diverge = false;
param.inner_product = 'gl';
param.tol_err = 5e-12;
param.tol_quad = 5e-10;                                                     % the default of 5e-16 is too stringent; nothing converges
param.verbose = 0;
param.m = 1000;
param = param_init_bfomfom(param);

nu = 100;
A100 = -dt*(D2 + nu*D1);
param.hermitian = false;
param = fomfom(A100,B,param);
exact_sol100 = param.Fm;
savestr = sprintf('conv_diff%g_exact_sol',nu);
save(savestr,'nu','A100','exact_sol100','B','N')

clear all %#ok<*CLALL> 

N = 350;                                                                    % build discretization matrix for 2D convection-diffusion problem
D2 = (N+1)^2*gallery('tridiag',N);
I = speye(N);
D2 = kron(I,D2) + kron(D2,I);
o = ones(N,1);
D1 = (N+1)/2*spdiags([-o,0*o,o],-1:1,N,N);
D1 = kron(I,D1) + kron(D1,I);
dt = 2*1e-3;                                                                % choose time step dt = 2*1e-3

n = N^2;
s = 10;
e = ones(n/s,1);                                                            % same RHS as for Laplacian
Is = eye(s);
B = kron(e,Is);

param.error_scale = 1;
param.function = 'exp';
param.halt_if_diverge = false;
param.inner_product = 'gl';
param.tol_err = 5e-12;
param.tol_quad = 5e-10;                                                     % the default of 5e-16 is too stringent; nothing converges
param.verbose = 0;
param.m = 1000;
param = param_init_bfomfom(param);

nu = 200;
A200 = -dt*(D2 + nu*D1);
param.hermitian = false;
param = fomfom(A200,B,param);
exact_sol200 = param.Fm;
savestr = sprintf('conv_diff%g_exact_sol',nu);
save(savestr,'nu','A200','exact_sol200','B','N')
