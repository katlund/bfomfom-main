function qcd_nonzero_chempotential_mod(caseID,modID,cycle_lengths,ipID,system_size)
% QCD_NONZERO_CHEMPOTENTIAL_MOD(modID,ipID) computes sign(Q)B for QCD
% matrices with nonzero chemical potential. For now, this code is adapted
% to the smaller QCD matrices.
%
% Running qcd_nonzero_chempotential_mod(3, 1:3, [2 5 10], 1:3, 'large') and
% max_cycles = qcd_nonzero_chempotential_counts(3, 'large') generates
% values for Table 5.1 in [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = [3 5];  % = mu*10
    modID = 1:3;
    cycle_lengths = [2 5 10];
    ipID = 1:3;
    system_size = 'small';
elseif nargin == 1
    modID = 1:3;
    cycle_lengths = [2 5 10];
    ipID = 1:3;
    system_size = 'small';
elseif nargin == 2
    cycle_lengths = [2 5 10];
    ipID = 1:3;
    system_size = 'small';
elseif nargin == 3
    ipID = 1:3;
    system_size = 'small';
elseif nargin == 4
    system_size = 'small';
end
if ischar(caseID)
    caseID = str2double(caseID);
end
if ischar(modID)
    modID = str2double(modID);
end
if ischar(cycle_lengths)
    cycle_lengths = str2double(cycle_lengths);
end
if ischar(ipID)
    ipID = str2double(ipID);
end
max_restarts = 3000;
tol_err = 1e-6;
Nquadstart = 150;
halt_if_diverge = true;
Nquadmax = 100000;
verbose = 1;

% Prepare for saving files
teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
mkdir(sprintf('%s/conv_plots',teststr));
mkdir(sprintf('%s/quad_plots',teststr));

% String options
ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%%
for c = caseID
    max_cycles = zeros(3,num_ips,max(cycle_lengths));                       % storage for max_cycle plots
    Nquad_total_count = max_cycles;
    loadstr = sprintf('data/qcd_%d_chempotential_exact_sol_%s.mat',c,system_size);
    if isfile(loadstr)
        data = load(loadstr);
    else
        inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
        decision = input(inputstr);
        if decision == 1
            fprintf('The missing .mat file will be generated locally.\n');
            qcd_nonzero_chempotential_exact_sol(c/10,system_size)
            data = load(loadstr);
        elseif decision == 0
            fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
            urlstr = sprintf('https://zenodo.org/record/6417952/files/qcd_%d_chempotential_exact_sol_%s.mat?download=1',...
                c,system_size);
            data = load(websave(loadstr, urlstr));
        end
    end
    data = load(loadstr, 'DWmu_squared', 'Bmu', 'exact_solmu');
    A = data.DWmu_squared;
    B = data.Bmu;
    exact_sol = data.exact_solmu;
    clear data
    
    [n,s] = size(B);
    hermitian = false;
    
    for m = cycle_lengths
        % Adapt tol_quad to the cycle length; shorter cycle lengths need a
        % lower tolerance
        if m < 5
            tol_quad = tol_err/1000;
        elseif m >= 5 && m <= 10
            tol_quad = tol_err/100;
        elseif m > 10
            tol_quad = tol_err/10;
        end
        for mod = modID
%             fig_conv = figure; ax_conv = gca; hold on;
%             fig_quad = figure; ax_quad = gca; hold on;
            if mod == 3
                scl = 1.001;
                Theta = scl*eigs(A,1)*eye(s);      % choose eigenvalue to prescribe
            end

            for ip = ipID
                method_str{mod,ip} = ...
                    sprintf('%s-%s',ip_str{ip},mod_str{mod});               % store labels for legend
                fprintf('--------------------QCD: case %d, %s, m = %d, s = %d --------------------\n',...
                    c,method_str{mod,ip},m,s);

                param = [];
                param.exact = exact_sol;
                param.error_scale = norm(param.exact,'fro');
                param.function = 'invSqrt';
                param.hermitian = hermitian;
                param.inner_product = ip_options{ip};

                if ~strcmp(mod_options{mod},'none')
                    param.mod = mod_options{mod};
                end
                if mod == 3
                    param.Theta = Theta;
                end

                param.max_restarts = max_restarts;
                param.halt_if_diverge = halt_if_diverge;
                param.norm = 'fro';
                param.Nquad1 = Nquadstart;                                  % especially important for prescribed Arnoldi to start a bit higher than default
                param.Nquadmax = Nquadmax;                                  % global needs a lot of nodes!
                param.restart_length = m;
                param.tol_err = tol_err;
                param.tol_quad = tol_quad;
                param.verbose = verbose;

                param = param_init_bfomfom(param);                          % set remaining parameters

                param = bfomfom(A,B,param);                                 % exectute B(FOM)^2!
                exact_error = param.exact_error;
                Nquad_per_cycle = param.Nquad_per_cycle;
                
                if halt_if_diverge
                    if param.flag == 1 || param.flag == 3
                        fprintf('%s: %d\n\n',method_str{mod,ip},length(exact_error));
                        max_cycles(mod,ip,m) = length(exact_error);
                        Nquad_total_count(mod,ip,m) = sum(Nquad_per_cycle);
                    else
                        fprintf('%s: NaN\n\n',method_str{mod,ip});
                        max_cycles(mod,ip,m) = NaN;
                        Nquad_total_count(mod,ip,m) = NaN;
                    end
                else
                    fprintf('%s: %d\n\n',method_str{mod,ip},length(exact_error));
                    max_cycles(mod,ip,m) = length(exact_error);
                    Nquad_total_count(mod,ip,m) = sum(Nquad_per_cycle);
                end
                
                savenowstr = sprintf('%s/case%d_%s_%d_%d_%d',...
                    teststr,c,method_str{mod,ip},n,m,s);
                save(savenowstr,...
                    'exact_error','Nquad_per_cycle',...
                    'max_cycles','Nquad_total_count');

%                 % Plot convergence curve
%                 semilogy(ax_conv,0:length(exact_error),[1 exact_error],...
%                     ip_markers{ip}(1:end-1),'MarkerSize',2,...
%                     'Color',colors{ip},'LineWidth',linewidth);
% 
%                 % Plot quadrature count
%                 plot(ax_quad,[0 Nquad_per_cycle],...
%                     ip_markers{ip}(1:end-1),'MarkerSize',2,...
%                     'Color',colors{ip},'LineWidth',linewidth);
            end

%             % Touch up plots
%             xlabel(ax_conv,'cycle index')
%             grid on
%             set(ax_conv,'FontSize',18,'YScale','log',...
%                 'YMinorGrid','off','XMinorGrid','off')
%             legend(ax_conv,method_str(mod,:),'Location','Best')
% 
%             xlabel(ax_quad,'cycle index')
%             grid on
%             set(ax_quad,'FontSize',18,'YScale','log',...
%                 'YMinorGrid','off','XMinorGrid','off')
%             legend(ax_quad,method_str(mod,:),'Location','Best')
% 
%             % Save plots
%             savestr = sprintf('%s/conv_plots/case%d_%s_%d_%d_%d',...
%                 teststr,c,mod_str{mod},n,m,s);                              % string for saving .fig and .eps files
%             savefig(fig_conv,savestr);
%             saveas(fig_conv,savestr,'epsc');
%             close(fig_conv);
% 
%             savestr = sprintf('%s/quad_plots/case%d_%s_%d_%d_%d',...
%                 teststr,c,mod_str{mod},n,m,s);                              % string for saving .fig and .eps files
%             savefig(fig_quad,savestr);
%             saveas(fig_quad,savestr,'epsc');
%             close(fig_quad);
        end
    end
end
end