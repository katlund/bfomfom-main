function nonnorm_nondiag(caseID, ipID)
% NONNORM_NONDIAG(caseID, ipID) runs modified B(FOM)^2 on nonnormal,
% nondiagonal matrices. In particular, we estimate A^{-1/2}B, where A is a
% 1000 x 1000 matrix and B = qr(rand(n,s),0).
%
% Although the outputs of this test are not reported in
% [FrommerLundSzyld2020], they may still be of interest, as they can be
% compared with smaller scale tests in
%
% A. Frommer, S. G�ttel, and M. Schweitzer: Convergence of restarted Krylov
% subspace methods for Stieltjes functions of matrices, SIAM Journal on
% matrix Analysis and Applications, Vol. 35, 4, pp. 1602-1624, 2014.
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = 1:2;                                                           % 1 = [.6,.8], 2 = [.5001,.5099]
    ipID = 1:3;                                                             % which inner products to run
elseif nargin == 1
    ipID = 1:3;
end
if ischar(caseID)
    caseID = str2double(caseID);                                            % fixes a problem with executing Matlab in an external command line
end

% Set parameters
hermitian = false;
m = 20;
max_restarts = 100;
halt_if_diverge = true;
tol_err = 1e-10;
tol_quad = tol_err/10;
verbose = 1;

% Load data and prepare for saving files
loadstr = 'data/nonnorm_nondiag_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    fprintf('File %s is missing.\n It will be generated locally.\n', loadstr);
    nonnorm_nondiag_exact_sol
    data = load(loadstr);
end
A = data.A;
B = data.B;
exact_sol = data.exact_sol;
clear data

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);

% String options
ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

modID = 1:3;
mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%%
for c = caseID
    if c == 1
        Theta = .8 + 10*1i;
    elseif c == 2
        Theta = .5099 + 10*1i;
    end
    scl = 1.01;
    Theta = 1.01*Theta*eye(s);                                              % prescribe solvent for RA
    for mod = modID
        cla; hold on;
        
        % To make plots pretty
        minx = 0; maxx = 0;
        miny = tol_err; maxy = 1;
        
        for ip = ipID
            fprintf('--------------------NN: case %d, %s-%s, m = %d, s = %d --------------------\n',...
                c,ip_str{ip},mod_str{mod},m,s);
            method_str{mod,ip} = ...
                sprintf('%s-%s',ip_str{ip},mod_str{mod});                   % store labels for legend
            param = [];
            param.exact = exact_sol{c};
            param.error_scale = norm(param.exact,'fro');
            param.function = 'invSqrt';
            param.hermitian = hermitian;
            param.inner_product = ip_options{ip};
            
            if ~strcmp(mod_options{mod},'none')
                param.mod = mod_options{mod};
            end
            if mod == 3
                param.Theta = Theta;
            end
            
            param.max_restarts = max_restarts;
            param.halt_if_diverge = halt_if_diverge;
            param.norm = 'fro';
            param.restart_length = m;
            param.tol_err = tol_err;
            param.tol_quad = tol_quad;
            param.verbose = verbose;
            
            param = param_init_bfomfom(param);                              % set remaining parameters
            
            param = bfomfom(A{c},B,param);                                  % execute B(FOM)^2!
            
            % Plot each method
            maxx = max(maxx,length(param.exact_error));
            miny = min(miny,param.exact_error(end));
            semilogy(0:length(param.exact_error),[1 param.exact_error],...
                ip_markers{ip}(1:end-1),'MarkerSize',2,...
                'Color',colors{ip},'LineWidth',linewidth);
%             savestr = sprintf('%s/case%d_%s_%s_%d_%d_%d',...
%                 teststr,c,ip_str{ip},mod_str{mod},n,m,s);                   % string for saving .mat file
%             save(savestr,'param')
        end
        
        % Touch up plots
        axis([minx maxx miny maxy])
        xlabel('cycle index')
        grid on
        set(gca,'FontSize',22,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        legend(method_str(mod,:),'Location','Best')

        % Save plots
        savestr = sprintf('%s/case%d_%s_%d_%d_%d',...
                teststr,c,mod_str{mod},n,m,s);                              % string for saving .fig and .eps files
        savefig(gcf,savestr);                                               % save .fig file for references
        saveas(gcf,savestr,'epsc');                                         % save .eps file for paper
        close(gcf);
    end
end
end