function invSqrt_mod_plots(caseID,modID,ipID)
% INVSQRT_MOD(caseID,ipID,modID) renders plots from invSqrt_mod.m
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = [1 4];
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 1
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 2
    ipID = 1:3;
end

n = 5000;
m = 25;     % cycle length
num_RHS = 10; % number of right-hand sides

% Prepare for saving files
teststr = sprintf('results/invSqrt_mod');
mkdir(teststr);
mkdir(sprintf('%s/conv_plots',teststr));
mkdir(sprintf('%s/quad_plots',teststr));

% String options
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%% Run algorithms
for c = caseID
    for mod = modID
        fig_conv = figure; ax_conv = gca; hold on;
        fig_quad = figure; ax_quad = gca; hold on;
        
        for ip = ipID
            for s = num_RHS
                method_str{mod,ip} = ...
                    sprintf('%s-%s',ip_str{ip},mod_str{mod});               % store labels for legend
                
                loadstr = sprintf('%s/case%d_%s_%d_%d_%d.mat',...
                    teststr,c,method_str{mod,ip},n,m,s);
                if isfile(loadstr)
                    load(loadstr,'exact_error','Nquad_per_cycle');
                
                    % Plot convergence curve
                    semilogy(ax_conv,0:length(exact_error),[1 exact_error],...
                        ip_markers{ip}(1:end-1),'MarkerSize',2,...
                        'Color',colors{ip},'LineWidth',linewidth);

                    % Plot quadrature count
                    plot(ax_quad,[0 Nquad_per_cycle],...
                        ip_markers{ip}(1:end-1),'MarkerSize',2,...
                        'Color',colors{ip},'LineWidth',linewidth);
                end
            end
        end
        
        % Touch up plots
        xlabel(ax_conv,'cycle index')
        grid(ax_conv,'on')
        set(ax_conv,'FontSize',18,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off',...
            'YLim',[1e-6,1])
        axis(ax_conv,'square')
        legend(ax_conv,method_str(mod,:),'Location','Best')

        xlabel(ax_quad,'cycle index')
        grid(ax_quad,'on')
        set(ax_quad,'FontSize',18,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        axis(ax_quad,'square')
        legend(ax_quad,method_str(mod,:),'Location','Best')

        % Save plots
        savestr = sprintf('%s/conv_plots/case%d_%s_%d_%d_%d',...
            teststr,c,mod_str{mod},n,m,s);                                  % string for saving .fig and .eps files
        savefig(fig_conv,savestr);
        saveas(fig_conv,savestr,'epsc');
        close(fig_conv);

        savestr = sprintf('%s/quad_plots/case%d_%s_%d_%d_%d',...
            teststr,c,mod_str{mod},n,m,s);                                  % string for saving .fig and .eps files
        savefig(fig_quad,savestr);
        saveas(fig_quad,savestr,'epsc');
        close(fig_quad);
    end
end
end