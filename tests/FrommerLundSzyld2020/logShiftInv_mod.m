function logShiftInv_mod(caseID,modID,ipID)
% LOGSHIFTINV_MOD(caseID,ipID,modID) computes logShiftInv(A)B for diagonal
% matrices saved in the eponymous data file.
%
% Running logShiftInv_mod(4, 1:3, 1:3) generates FIg. 5.5 in
% [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = [1 2 4];
    modID = 1:3;                                                            % which modifications to run
    ipID = 1:3;                                                             % which inner products to run
elseif nargin == 1
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 2
    ipID = 1:3;
end
if ischar(caseID)
    caseID = str2double(caseID);                                            % fixes a problem with executing Matlab in an external command line
end
if ischar(modID)
    modID = str2double(modID);                                              % fixes a problem with executing Matlab in an external command line
end
if ischar(ipID)
    ipID = str2double(ipID);                                                % fixes a problem with executing Matlab in an external command line
end

m = 15;     % cycle length
max_restarts = 60;
num_RHS = 10;
tol_err = 1e-6;
Nquadstart = 150;
halt_if_diverge = true;
Nquadmax = 100000;
verbose = 1;

% Prepare for saving files
teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
mkdir(sprintf('%s/conv_plots',teststr));
mkdir(sprintf('%s/quad_plots',teststr));

% String options
ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%% Load data
n = 5000;
s = 100;
loadstr = sprintf('data/%s_setup_%d_%d.mat',mfilename,n,s);
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        logShiftInv_mod_setup(n,s)
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6417952/files/logShiftInv_mod_setup_5000_100.mat?download=1'));
    end
end
lamA = data.lamA;
A = data.A;
B = data.B;
exact_sol = data.exact_sol;
hermitian = data.hermitian;
clear data

%% Run algorithms
for c = caseID    
    for mod = modID
        fig_conv = figure; ax_conv = gca; hold on;
        fig_quad = figure; ax_quad = gca; hold on;
        
        for ip = ipID
            for s = num_RHS
                scl = 1.01; % how much to scale the prescribed Ritz value
                switch c
                    case {1, 2, 3, 8}
                        hermitian = true;
                    case {4, 5, 6, 7}
                        hermitian = false;
                end
                Theta = scl*max(real(lamA{c}))*eye(s);
                
                fprintf('--------------------Case %d, %s-%s, m = %d, s = %d --------------------\n',...
                            c,ip_str{ip},mod_str{mod},m,s);
                method_str{mod,ip} = ...
                    sprintf('%s-%s',ip_str{ip},mod_str{mod});               % store labels for legend

                param = [];
                param.exact = exact_sol{c}(:,1:s);
                param.function = 'logShiftInv';
                param.hermitian = hermitian;
                param.inner_product = ip_options{ip};

                if ~strcmp(mod_options{mod},'none')
                    param.mod = mod_options{mod};
                end
                if mod == 3
                    param.Theta = Theta;
                end

                param.max_restarts = max_restarts;
                param.halt_if_diverge = halt_if_diverge;
                param.norm = 'fro';
                param.Nquad1 = Nquadstart;                                  % especially important for prescribed Arnoldi to start a bit higher than default
                param.Nquadmax = Nquadmax;                                  % global needs a lot of nodes!
                param.error_scale = norm(param.exact,'fro');
                param.restart_length = m;
                param.tol_err = tol_err;
                param.tol_quad = tol_err/1000;
                param.verbose = verbose;

                param = param_init_bfomfom(param);                          % set remaining parameters

                param = bfomfom(A{c},B(:,1:s),param);                       % exectute B(FOM)^2!
                
                exact_error = param.exact_error;
                Nquad_per_cycle = param.Nquad_per_cycle;
                if halt_if_diverge
                    if param.flag == 1 || param.flag == 3
                        fprintf('%s: %d\n\n',method_str{mod,ip},length(exact_error));
                    else
                        fprintf('%s: NaN\n\n',method_str{mod,ip});
                    end
                else
                    fprintf('%s: %d\n\n',method_str{mod,ip},length(exact_error));
                end

                % Plot convergence curve
                semilogy(ax_conv,0:length(exact_error),[1 exact_error],...
                    ip_markers{ip}(1:end-1),'MarkerSize',2,...
                    'Color',colors{ip},'LineWidth',linewidth);

                % Plot quadrature count
                plot(ax_quad,[0 Nquad_per_cycle],...
                    ip_markers{ip}(1:end-1),'MarkerSize',2,...
                    'Color',colors{ip},'LineWidth',linewidth);
                
                savenowstr = sprintf('%s/case%d_%s_%d_%d_%d',...
                    teststr,c,method_str{mod,ip},n,m,s);
                save(savenowstr,'exact_error','Nquad_per_cycle');
            end
        end
        
        % Touch up plots
        xlabel(ax_conv,'cycle index')
        grid on
        set(ax_conv,'FontSize',18,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        axis square
        legend(ax_conv,method_str(mod,:),'Location','Best')

        xlabel(ax_quad,'cycle index')
        grid on
        set(ax_quad,'FontSize',18,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        axis square
        legend(ax_quad,method_str(mod,:),'Location','Best')

        % Save plots
        savestr = sprintf('%s/conv_plots/case%d_%s_%d_%d_%d',...
            teststr,c,mod_str{mod},n,m,s);                                  % string for saving .fig and .eps files
        savefig(fig_conv,savestr);
        saveas(fig_conv,savestr,'epsc');
        close(fig_conv);
 
        savestr = sprintf('%s/quad_plots/case%d_%s_%d_%d_%d',...
            teststr,c,mod_str{mod},n,m,s);                                  % string for saving .fig and .eps files
        savefig(fig_quad,savestr);
        saveas(fig_quad,savestr,'epsc');
        close(fig_quad);
    end
end
end