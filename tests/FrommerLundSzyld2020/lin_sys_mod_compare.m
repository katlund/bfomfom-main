function lin_sys_mod_compare(caseID,modID,ipID)
% LIN_SYS_MOD_COMPARE(caseID,modID,ipID) returns convergence plots for the
% specified tests.  See the eponymous data file for a description of each
% caseID, which must be a subset of 1:8. modID must be a subset of 1:3,
% where 1 = no modification, 2 = harmonic, and 3 = radau. ipID must be a
% subset of 1:5, where 1 = classical, 2 = global, 3 = loop-interchange, 4 =
% hybrid, and 5 = non-block.
%
% Running lin_sys_mod_compare(1:2, 1:3, 1:3) generates Fig. 5.1 in
% [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = [1 2];
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 1
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 2
    ipID = 1:3;
end

verbose = 1;                                                                % whether the user wants to print intermediate information to screen
halt_if_diverge = 1;                                                        % whether we expect monotone convergence (not guaranteed for FOM or RA on non-HPD matrices)

n = 5000;
s = 100;
loadstr = sprintf('data/lin_sys_mod_setup_%d_%d.mat',n,s);
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        lin_sys_mod_setup(n,s)
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6417952/files/lin_sys_mod_setup_5000_100.mat?download=1'));
    end
end
lamA = data.lamA;
A = data.A;
B = data.B;
exact_sol = data.exact_sol;
condA = data.condA;
hermitian = data.hermitian;
clear data

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);

% Generate 
cycle_length = 1:50;
num_RHS = 10;
max_restarts = 1;
tol_err = 1e-10;

% To make plots pretty
minx = 0;
miny = tol_err; maxy = 1;

ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'FOM','GMRES', 'RA'};
norm_options = {'A-fro','harmonic','radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

for c = caseID
    lmax = max(real(lamA{c})); lmin = min(real(lamA{c}));
    for mod = modID
        for r = num_RHS
            Theta = lmax*1.01*eye(r);                                       % prescribe solvent for RA
            clf; hold on;
            
            for ip = ipID
                exact_error = 0*cycle_length;
                
                for m = cycle_length
                    method_str{mod,ip} = ...
                        sprintf('%s-%s',ip_str{ip},mod_str{mod});           % store labels for legend
                    param = [];
                    param.exact = exact_sol{c}(:,1:r);
                    param.hermitian = hermitian(c);
                    param.inner_product = ip_options{ip};

                    if ~strcmp(mod_options{mod},'none')                     % set modification parameters
                        param.mod = mod_options{mod};
                    end
                    param.Theta = Theta;                                    % set prescribed solvent for RA

                    param.max_restarts = max_restarts;
                    if hermitian(c) || (~hermitian(c) && strcmp(mod_options{mod},'harmonic'))
                        param.norm = norm_options{mod};
                        param.error_scale = block_norm(param.exact,param,A{c});
                    else
                        param.norm = 'fro';
                        param.error_scale = norm(param.exact,'fro');
                    end

                    param.halt_if_diverge = halt_if_diverge;
                    param.restart_length = m;
                    param.tol_err = tol_err;
                    param.verbose = verbose;

                    param = param_init_bfom(param);                         % set remaining parameters

                    param = bfom(A{c},B(:,1:r),param);                      % execute that linear solver!

                    exact_error(m) = param.exact_error(end);
                end
                semilogy(0:length(exact_error),[1 exact_error],...
                    ip_markers{ip}(1:end-1),'MarkerSize',2,...
                    'Color',colors{ip},...
                    'LineWidth',linewidth);
            end
            %% Finish up plots
            axis square
            xlabel('cycle length')
            grid on
            set(gca,'FontSize',18,'YScale','log',...
                'YMinorGrid','off','XMinorGrid','off',...
                'XLim',[0 50])
            legend(method_str(mod,:),'Location','Best')

            % Save plots
            savestr = sprintf('%s/case%d_%s_%d_m_%d',...
                teststr,c,mod_str{mod},n,r);                                % string for saving .fig and .eps files
            savefig(gcf,savestr);                                           % save .fig file for references
            saveas(gcf,savestr,'epsc');                                     % save .eps file for paper
            close(gcf);
        end
    end
end
end
