% NONNORM_NONDIAG_EXACT_SOL generates and saves exact solutions for the
% nonnormal, nondiagonal examples.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
n = 1000;
s = 10;

A = cell(1,2);
o = kron(ones(n/2,1),[1; 0]);

% A1
xmin = .6; xmax = .8;
ymin = -10; ymax = 10;
x = xmin + (xmax-xmin).*rand(n/2,1);
y = ymin + (ymax-ymin).*rand(n/2,1);
lam1 = kron(x + 1i*y,[1;1]);
A{1} = spdiags([o lam1],-1:0,n,n);

% A2
xmin = .5001; xmax = .5099;
ymin = -10; ymax = 10;
x = xmin + (xmax-xmin).*rand(n/2,1);
y = ymin + (ymax-ymin).*rand(n/2,1);
lam2 = kron(x + 1i*y,[1;1]);
A{2} = spdiags([o lam2],-1:0,n,n);

% B = eye(n);
% B = B(:,1:s);
[B,~] = qr(rand(n,s),0);
exact_sol = cell(1,2);
exact_sol{1} = sqrtm(full(A{1}))\B;
exact_sol{2} = sqrtm(full(A{2}))\B;
%%
savestr = sprintf('data/%s', mfilename);
save(savestr)