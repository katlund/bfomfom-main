function qcd_nonzero_chempotential_exact_sol(Mu,system_size)
% QCD_NONZERO_CHEMPOTENTIAL_EXACT_SOL(mu) prepares QCD matrices from the
% SuiteSparse collection and computes an exact solution to sign(Q)B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%% Base system
% For mu = 0 (no chemical potential), the correspondung matrix is DW0.
switch system_size
    case 'small'
        data = load(websave('data/tmp',...
            'https://suitesparse-collection-website.herokuapp.com/mat/QCD/conf6_0-4x4-30.mat'));
    case 'big'
        data = load(websave('data/tmp',...
            'https://suitesparse-collection-website.herokuapp.com/mat/QCD/conf6_0-8x8-30.mat'));
end
data = data.Problem;     % makes syntax a bit simpler
A = 0.5*data.A;          % need to scale by 1/2 (why? don't remember -- ask A. Frommer)
realmax = real(eigs(A,1,'largestreal','Tolerance',1e-3));
dim = size(A,1);
N = round((dim/12)^0.25);    % the lattice is N^4
kappa = 4/(3*realmax);
DW0 = speye(dim) + kappa*A;  % Wilson-Dirac without chemical potential

gamma5 = [0 0 1 0; 0 0 0 1; 1 0 0 0; 0 1 0 0];
Gamma5 = kron(speye(dim/12),kron(gamma5,speye(3)));

% % Compute true solution to system with zero chemical potential
% DW0_squared = (Gamma5*DW0)*(Gamma5*DW0); % squared "symmetrized" operators (Gamma5*DW)^2
% B0 = DW0*eye(dim,12);
% exact_sol0 = sqrtm(full(DW0_squared))\B0;
% save('qcd_0_chempotential_exact_sol','DW0_squared','B0','exact_sol0')

%% Nonzero potential
% Now include the non-zero chemical potential. This means that all coupling
% coefficients in the 1st dimension are multiplied by e^\mu in forward, and
% e^{-mu} in backward direction
% mu = 0.3;  % the value in the paper with Bloch et. al.; set to 0 for no chemical potential
if ischar(Mu)
    Mu = str2double(Mu);
end
for mu = Mu
    E = ones(12,12);
    S1 = speye(N);
    S = [S1(:,2:N), S1(:,1)];
    T = kron(speye(N^3),kron(S,E));

    % Now T marks the matrix pattern of the entries which are backwards in
    % dimension 1, the innermost one
    DWmu = DW0 + DW0.*T*(exp(mu)-1);
    S = [S1(:,N), S1(:,1:N-1) ];
    T = kron(speye(N^3),kron(S,E));
    % Now T marks the matrix pattern of the entries which are backwards in
    % dimension 1, the innermost one
    DWmu = DWmu + DW0.*T*(exp(-mu)-1);

    % Compute true solution to system with nonzero potential
    DWmu_squared  = (Gamma5*DWmu)*(Gamma5*DWmu); % squared "symmetrized" operator (Gamma5*DW)^2
    Bmu = DWmu*eye(dim,12);
    switch system_size
        case 'small'
            exact_solmu = sqrtm(full(DWmu_squared))\Bmu;
        case 'big'
            addpath('../../bfomfom')
            param = [];
            param.conv_check = 'approx';
            param.error_scale = norm(Bmu,'fro');
            param.function = 'invSqrt';
            param.halt_if_diverge = 0;
            param.hermitian = false;
            param.inner_product = 'cl_full';
            param.max_restarts = 1000;
            param.norm = 'fro';
            param.Nquad1 = 100;
            param.Nquad2 = round(sqrt(2)*param.Nquad1);
            param.Nquadmax = 100000;
            param.reduce_Nquad = 1;
            param.restart_length = 100;
            param.tol_err = 1e-16;
            param.tol_quad = param.tol_err/10;
            param = param_init_bfomfom(param);

            param = bfomfom(DWmu_squared,Bmu,param);
            exact_solmu = param.Fm;
    end
    savestr = sprintf('data/qcd_%d_chempotential_exact_sol_%s',round(10*mu),system_size);
    save(savestr,'DWmu_squared','Bmu','exact_solmu')
end