function invSqrt_mod_setup(n,s)
% INVSQRT_MOD_SETUP generates eight diagonal matrices and computes
% A^{-1/2}B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%% Generate set-up
if nargin == 0
    n = 5000;
    s = 100;
elseif nargin == 1
    s = 100;
end

lamstr = ...
    {'$\spec(A)$ uniformly spaced, $A = A^* > 0$',...                           % c = 1
    '$\spec(A)$ logarithmically spaced, $A = A^* > 0$',...                      % c = 2
    'half of $\spec(A)$ is log-spaced, half is uni-spaced',...                  % c = 3
    '$Re(\spec(A)) > 0$ uniformly spaced, $Im(\spec(A)) = -Im(\spec(A))$',...   % c = 4
    '$Re(\spec(A)) > 0$ uniformly spaced',...                                   % c = 5
    '$Re(\spec(A)) is half log-spaced, half uni-spaced',...                     % c = 6
    'Shifted unit circle with imaginary noise',...                              % c = 7
    'Laplacian'                                                                 % c = 8
    };

%% Eigenvalue distributions
lamA = cell(1,8);

% Hermitian positive definite matrices
lamA{1} = linspace(1e-2,1e2,n);
lamA{2} = logspace(-2,2,n);
lamA{3} = [logspace(-2,-1,n/2) linspace(1e1,1e2,n/2)];

% Real positive matrices
lamA{4} = linspace(1e-2,1e1,n/2) + 1i*sqrt(n)*rand(1,n/2);
    lamA{4} = [lamA{4} conj(lamA{4})];
lamA{5} = lamA{4} + 1i*sqrt(n)*rand(1,n);
lamA{6} = lamA{3} + 1i*10*rand(1,n);

theta = linspace(0,2*pi,n/2)*1i;
lamA{7} = [exp(theta) + sqrt(n) + 1i*(sqrt(n)/2)*rand(1,n/2) logspace(-2,1,n/2)];

% Laplacian
N = ceil(sqrt(n));
ev = (N+1)^2*eig_2d_poisson(N);
ind = randperm(N^2);
ev(ind(1:41)) = [];
lamA{8} = ev'/min(ev);

L = length(lamA);

[B,~] = qr(rand(n,s) + 1i*rand(n,s),0);
exact_sol = cell(1,L);
A = lamA;
condA = zeros(1,L);
hermitian = [1 1 1 0 0 0 0 0 1];

for c = 1:L
    A{c} = spdiags(transpose(lamA{c}),0,n,n);
    condA(c) = round(log10(condest(A{c})));
    exact_sol{c} = sqrt(A{c})\B;
end

savestr = sprintf('data/%s_%d_%d',mfilename,n,s);
save(savestr)
end