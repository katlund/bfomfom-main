% FIRST_TIME_MAT_FUNC_MOD runs bfomfom_mod for a small problem to estimate
% the solution to exp(A)B. It also introduces additional parameter
% specifications, compared to FIRST_TIME_MAT_FUNC.
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%%
addpath(genpath('../../'))

n = 100; s = 4;
A = -spdiags((1:n)',0,n,n);
B = randn(n,s);

ip_options = {'cl_defl', 'gl', 'li', 'nb','hy'};
ip_str = {'cl_defl', 'gl', 'li', 'nb', 'hy'};
mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'','-har', '-rad'};

param = [];
param.verbose = 1;                                                          % whether to print intermediate information
param.exact = expm(A)*B;                                                    % assign exact solution, if known
param.function = 'exp';                                                     % which function to approximate (needed to specify quadrature rule)
param.conv_check = 'exact';                                                 % how to measure convergence, i.e., with the exact error or the approximate error ('approx')
param.error_scale = norm(param.exact,'fro');                                % the value by which to scale the error
param.hermitian = 1;                                                        % whether A is hermitian; if true, then Lanczos routines are used
param.max_restarts = 100;                                                   % number of permitted restart cycles
param.halt_if_diverge = 0;                                                  % whether to halt the program if the error exceeds the initial error
param.norm = 'fro';                                                         % a string specifying the norm used to measure the error
param.Nquad1 = 100;                                                         % starting number of quadrature nodes
param.Nquad2 = round(sqrt(2)*param.Nquad1);                                 % starting number of quadrature nodes for adaptive rule
param.Nquadmax = 50000;                                                     % maximum number of quadrature nodes allowed
param.reduce_Nquad = 1;                                                     % whether to reduce the number of nodes per cycle
param.tol_err = 1e-6;                                                       % desired accuracy
param.tol_quad = param.tol_err;                                             % desired accuracy for quadrature rule
param.tol_zero = 1e-13;                                                     % what is considered 0 for the deflation routines

param.verbose = 0;                                                          % whether to print intermediate information
for ip = 1:5
    for mod = 1:3
        param.inner_product = ip_options{ip};                               % choice of block inner product
        if strcmp(ip_options{ip},'hy')
            param.q = 2;                                                    % specify block size for hybrid inner product
        end
        
        param.mod = mod_options{mod};                                       % choice of modification
        if strcmp(mod_options{mod},'radau-arnoldi')
            param.Theta = -n*eye(s);                                        % choose solvent to fix for radau modification
        end
        
        param.restart_length = 5;                                           % number of basis vectors before restart
    param = param_init_bfomfom(param);                                      % initialize all other parameter fields
    param = bfomfom(A,B,param);
    fprintf('%s%s: param.Fm approximates expm(A)B with accuracy %g in %d cycles.\n\n',...
        ip_str{ip},mod_str{mod},param.exact_error(end),length(param.exact_error))
    end
end