function conv_diff_mod(caseID,ipID,modID)
% CONV_DIFF_MOD(caseID,ipID,modID) computes exp(A)B for the 2D
% convection-diffusion equation and reports the maximum number of
% quadrature nodes needed per cycle. caseID = 0, 100, or 200, each
% corresponding to the value of the convection coefficient.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = [0 100 200];                                                   % different values for nu
    ipID = 1:3;                                                             % which inner products to run
    modID = 1:3;                                                            % which modificationst to run
elseif nargin == 1
    ipID = 1:3;
    modID = 1:3;                                                            % which modificationst to run
elseif nargin == 2
    modID = 1:3;
end
if ischar(caseID)
    caseID = str2double(caseID);                                            % fixes a problem with executing Matlab in an external command line
end
if ischar(ipID)
    ipID = str2double(ipID);                                                % fixes a problem with executing Matlab in an external command line
end
if ischar(modID)
    modID = str2double(modID);                                              % fixes a problem with executing Matlab in an external command line
end
m = 20;                                                                     % m = 50 in ETNA example, but we want to challenge the global method
max_restarts = 100;
tol_err = 1e-6;                                                             % 1e-6 in ETNA example
Nquadstart = 150;
halt_if_diverge = true;
Nquadmax = 100000;
verbose = 0;

%% Calculate range of eigenvalues for RA
% I'm sure there's theory behind why this gives a good eigenvalue
% approximation, but I mostly did some reverse engineering and trial and
% error
N = 350; % n = N^2;
dt = 2*1e-3;

ev0 = -dt*(N+1)^2*eig_2d_poisson(N); % eigenvalues of poisson operator
med_ev0 = median(ev0); % median of poisson eigenvalues

o = ones(N,1);
D1 = (N+1)/2*spdiags([-o,0*o,o],-1:1,N,N);  % building block of convection matrix
evD1 = eig(full(D1)); [X,Y] = meshgrid(evD1,evD1);
evD1 = reshape(X + Y,N^2,1);  % eigenvalues of convection matrix

ev100 = imag(-dt*100*evD1 + ev0)*1i + med_ev0; % eigenvalues for nu = 100
ev200 = imag(-dt*200*evD1 + ev0)*1i + med_ev0; % eigenvalues for nu = 200

% Note that in the convection cases, the eigenvalues are very close to
% pure imaginary.

% Prepare for saving files
teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
mkdir(sprintf('%s/conv_plots',teststr));
mkdir(sprintf('%s/quad_plots',teststr));

% String options
ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%%
for nu = caseID    
    loadstr = sprintf('data/conv_diff%g_exact_sol.mat',nu);
    if isfile(loadstr)
        data = load(loadstr);
    else
        inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
        decision = input(inputstr);
        if decision == 1
            fprintf('The missing .mat file will be generated locally.\n');
            conv_diff_exact_sol;
            data = load(loadstr);
        elseif decision == 0
            fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
            downloadstr = sprintf('https://zenodo.org/record/6414224/files/conv_diff%d_exact_sol.mat?download=1',nu);
            data = load(websave(loadstr, downloadstr));
        end
    end
    Astr = sprintf('A%g',nu);
    exactstr = sprintf('exact_sol%g',nu);
    data = load(loadstr,Astr,'B',exactstr);
    [B,Beta] = qr(data.B,0);                                                % see if this eliminates issue with relative error
    [n,s] = size(B);
    
    scl = 1.1; % how much to scale the prescribed Ritz value
    switch nu
        case 0
            A = data.A0;
            hermitian = true;
            exact_sol = data.exact_sol0/Beta;
            Theta = min(ev0)*scl*eye(s);
%             Theta = med_ev0*eye(s);  % no convergence
            
        case 100
            A = data.A100;
            hermitian = false;
            exact_sol = data.exact_sol100/Beta;
            Theta = (min(imag(-dt*100*evD1 + ev0))*scl*1i + med_ev0)*eye(s);
%             Theta = min(ev0)*scl*eye(s);  % not very good convergence
            
        case 200
            A = data.A200;
            hermitian = false;
            exact_sol = data.exact_sol200/Beta;
            Theta = (min(imag(-dt*200*evD1 + ev0))*scl*1i + med_ev0)*eye(s);
%             Theta = min(ev0)*scl*eye(s); % not very good convergence
    end
    clear data  % problem is super big, need to make space on the average laptop
    
    for mod = modID
        fig_conv = figure; ax_conv = gca; hold on;
        fig_quad = figure; ax_quad = gca; hold on;
        
        for ip = ipID
            fprintf('--------------------CD: case %d, %s-%s, m = %d, s = %d --------------------\n',...
                nu,ip_str{ip},mod_str{mod},m,s);
            method_str{mod,ip} = ...
                sprintf('%s-%s',ip_str{ip},mod_str{mod});                 % store labels for legend
    
            param = [];
            param.exact = exact_sol;
            param.function = 'exp';
            param.hermitian = hermitian;
            param.inner_product = ip_options{ip};
            
            if ~strcmp(mod_options{mod},'none')
                param.mod = mod_options{mod};
            end
            if mod == 3
                param.Theta = Theta;
            end
            
            param.max_restarts = max_restarts;
            param.halt_if_diverge = halt_if_diverge;
            param.norm = 'fro';
            param.Nquad1 = Nquadstart;                                      % especially important for prescribed Arnoldi to start a bit higher than default
            param.Nquadmax = Nquadmax;                                      % global needs a lot of nodes!
            param.error_scale = norm(param.exact,'fro');
            param.restart_length = m;
            param.tol_err = tol_err;
            param.tol_quad = tol_err;
            param.verbose = verbose;
            
            param = param_init_bfomfom(param);                              % set remaining parameters
            
            param = bfomfom(A,B,param);                                     % exectute B(FOM)^2!
            if halt_if_diverge
                if param.flag == 1 || param.flag == 3
                    fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error));
                else
                    fprintf('%s: NaN\n\n',method_str{mod,ip});
                end
            else
                fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error));
            end
            
            % Plot convergence curve
            semilogy(ax_conv,0:length(param.exact_error),[1 param.exact_error],...
                ip_markers{ip}(1:end-1),'MarkerSize',2,...
                'Color',colors{ip},'LineWidth',linewidth);
            
            % Plot quadrature count
            plot(ax_quad,[0 param.Nquad_per_cycle],...
                ip_markers{ip}(1:end-1),'MarkerSize',2,...
                'Color',colors{ip},'LineWidth',linewidth);
                
%             savestr = sprintf('%s/conv_plots/case%d_%s_%s_%d_%d_%d',...
%                 teststr,nu,ip_str{ip},mod_str{mod},n,m,s);                  % string for saving .mat file
%             save(savestr,'param')                              % too big to save
        end
        
        % Touch up plots
        xlabel(ax_conv,'cycle index')
        grid on
        set(ax_conv,'FontSize',22,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        legend(ax_conv,method_str(mod,:),'Location','Best')

        xlabel(ax_quad,'cycle index')
        grid on
        set(ax_quad,'FontSize',22,'YScale','log',...
            'YMinorGrid','off','XMinorGrid','off')
        legend(ax_quad,method_str(mod,:),'Location','Best')

        % Save plots
        savestr = sprintf('%s/conv_plots/case%d_%s_%d_%d_%d',...
            teststr,nu,mod_str{mod},n,m,s);                                 % string for saving .fig and .eps files
        savefig(fig_conv,savestr);
        saveas(fig_conv,savestr,'epsc');
        close(fig_conv);

        savestr = sprintf('%s/quad_plots/case%d_%s_%d_%d_%d',...
            teststr,nu,mod_str{mod},n,m,s);                                 % string for saving .fig and .eps files
        savefig(fig_quad,savestr);
        saveas(fig_quad,savestr,'epsc');
        close(fig_quad);
    end
end
end