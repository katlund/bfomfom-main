function lapl_2d_mod(caseID,ipID,modID)
% LAPL_2D_MOD(caseID,ipID,modID) computes A^{-1/2}B for the 2D Laplacian
% for varying restart_lengths and two different right-hand sides-- one full
% rank (caseID = 1), and one which leads to deflation (caseID = 2).
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

if nargin == 0
    caseID = 1:2;                                                           % 1 = full rank; 2 = rank-deficient
    ipID = 1:3;                                                             % which inner products to run
    modID = 1:3;                                                            % which modifications to run
elseif nargin == 1
    ipID = 1:3;
    modID = 1:3;
elseif nargin == 2
    modID = 1:3;
end
if ischar(caseID)
    caseID = str2double(caseID);                                            % fixes a problem with executing Matlab in an external command line
end
if ischar(ipID)
    ipID = str2double(ipID);                                                % fixes a problem with executing Matlab in an external command line
end
if ischar(modID)
    modID = str2double(modID);
end
cycle_lengths = 20:35;  % ETNA example needed m = 25
max_restarts = 100;
halt_if_diverge = true;
tol_err = 1e-6;  % trying to mimic ETNA example
verbose = 0;

loadstr = 'data/lapl_2d_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        lapl_2d_exact_sol
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6414224/files/lapl_2d_exact_sol.mat?download=1'));
    end
end
[n,s] = size(data.B1);
B{1} = data.B1; exact_sol{1} = data.exact_sol1;
B{2} = data.B2; exact_sol{2} = data.exact_sol2;
A = data.A;
clear data

% Get exact eigenvalues of A for Radau approximation
ev = (sqrt(n)+1)^2*eig_2d_poisson(sqrt(n)); ev = ev/min(ev);
lmax = max(ev);
lmin = min(ev);

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
mkdir(sprintf('%s/cycle_plots',teststr));
mkdir(sprintf('%s/quad_cycle_plots',teststr));

ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

%% Save outputs
for c = caseID
    max_cycles = zeros(3,num_ips,max(cycle_lengths));                       % storage for max_cycle plots
    Nquad_total_count = max_cycles;
    for mod = modID      
        Theta = lmax*1.01*eye(s);                                           % prescribe solvent for RA

        for m = cycle_lengths
            for ip = ipID
                fprintf('--------------------L2D: case %d, %s-%s, m = %d, s = %d --------------------\n',...
                    c,ip_str{ip},mod_str{mod},m,s);
                method_str{mod,ip} = ...
                    sprintf('%s-%s',ip_str{ip},mod_str{mod});               % store labels for legend
                
                param = [];
                param.exact = exact_sol{c};
                param.function = 'invSqrt';
                param.hermitian = true;
                param.inner_product = ip_options{ip};
                
                if ~strcmp(mod_options{mod},'none')
                    param.mod = mod_options{mod};
                end
                if mod == 3
                    param.Theta = Theta;
                end
                
                param.max_restarts = max_restarts;
                param.halt_if_diverge = halt_if_diverge;
                param.norm = 'fro';
                param.error_scale = norm(param.exact,'fro');
                param.restart_length = m;
                param.tol_err = tol_err;
                param.verbose = verbose;
                
                param = param_init_bfomfom(param);                          % set remaining parameters
                
                param = bfomfom(A,B{c},param);                              % execute B(FOM)^2!
                
                if halt_if_diverge
                    if param.flag == 1 || param.flag == 3                   % distinguish between failure to converge and exhausting cycle allowance
                        fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error))
                        max_cycles(mod,ip,m) = length(param.exact_error);
                        Nquad_total_count(mod,ip,m) = sum(param.Nquad_per_cycle);
                    else
                        fprintf('%s: NaN\n\n',method_str{mod,ip})
                        max_cycles(mod,ip,m) = NaN;
                        Nquad_total_count(mod,ip,m) = NaN;
                    end
                else
                    fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error))
                    max_cycles(mod,ip,m) = length(param.exact_error);
                    Nquad_total_count(mod,ip,m) = sum(param.Nquad_per_cycle);
                end
            end
        end
        %% Plotting
        fig_conv = figure; ax_conv = gca; hold on;
%         fig_quad = figure; ax_quad = gca; hold on;
        for ip = ipID
            plot(ax_conv,cycle_lengths,squeeze(max_cycles(mod,ip,cycle_lengths)),...
                ip_markers{ip},'MarkerSize',3,...
                'Color',colors{ip},...
                'LineWidth',linewidth);

%             plot(ax_quad,cycle_lengths,squeeze(Nquad_total_count(mod,ip,cycle_lengths)),...
%                 ip_markers{ip},'MarkerSize',3,...
%                 'Color',colors{ip},...
%                 'LineWidth',linewidth);
        end
        xlabel(ax_conv,'cycle length')
        grid on
        set(ax_conv,'FontSize',22,...
            'YMinorGrid','off','XMinorGrid','off')
        legend(ax_conv,method_str(mod,:),'Location','Best')

%         xlabel(ax_quad,'cycle length')
%         grid on
%         set(ax_conv,'FontSize',22,...
%             'YMinorGrid','off','XMinorGrid','off')
%         legend(ax_quad,method_str(mod,:),'Location','Best')

        savestr = sprintf('%s/cycle_plots/case%d_%s_%d_%d',...
            teststr,c,mod_str{mod},n,s);
        savefig(fig_conv,savestr);
        saveas(fig_conv,savestr,'epsc');
        close(fig_conv);

%         savestr = sprintf('%s/quad_cycle_plots/case%d_%s_%d_%d',...
%             teststr,c,mod_str{mod},n,s);
%         savefig(fig_quad,savestr);
%         saveas(fig_quad,savestr,'epsc');
%         close(fig_quad);
        
        save(savestr,'max_cycles','Nquad_total_count')                      % save .mat files
    end
end
end