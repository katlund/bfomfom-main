function lin_sys_mod(output,caseID,modID,ipID)
% LIN_SYS_MOD(output,caseID,modID,ipID) executes a variety of tests
% depending on which output is selected. See the eponymous data file for a
% description of each caseID, which must be a subset of 1:8. modID must be
% a subset of 1:3, where
%   1 = no modification,
%   2 = harmonic,
%   3 = radau-arnoldi;
% ipID must be a subset of 1:5, where
%   1 = classical,
%   2 = global,
%   3 = loop-interchange,
%   4 = hybrid,
%   5 = non-block.
%
% output = 'conv_plots' returns traditional convergence histories for each
% caseID-modID-ipID configuration. The error is reported in the norm
% corresponding to the modification. (In the case of GMRES, it can also be
% interpreted as the residual.) When not specified, caseID = [1 2 4] (i.e.,
% all matrices in lin_sys_mod_setup), modID = 1:3 (i.e., non-modified,
% harmonic, and radua-arnoldi), and ipID = 1:3 (i.e., classical, global,
% and loop-interchange).
%
% output = 'cycle_plots_vary_m' varies the cycle length m and reports the
% number of cycles required to converge for each caseID-modID-ipID
% configuration and each cycle length in the range 10:5:50.  Defaults are
% caseID = [1 2 4]; modID = 1:3; ipID = 1:3.
%
% output = 'cycle_plots_vary_s' varies the number of right-hand sides s and
% reports the number of cycles needed to converge for each
% caseID-modID-ipID configuration and each s in the range 5:5:20.   Defaults are
% caseID = [1 2 4]; modID = 1:3; ipID = 1:3.
%
% output = 'poly_plots' generates plots of the residual polynomials for the
% first cycle for each caseID-modID-ipID configuration. Only one polynomial
% is plotted in each window, and the zeros are highlighted. When not
% specified, caseID = [1:3 8] and ipID = 1:3.
%
% Results are saved in subfolders specified by this filename and the output
% string, and the file-naming format is of the form caseID_ipID_n_m_s.
%
% Running
% lin_sys_mod('conv_plots',[1 2 4], 1:3, 1:3)
% will generate the top row of plots in Fig. 5.2 and the plots in Fig. 5.3
% in [FrommerLundSzyld2020].  Running
% lin_sys_mod('cycle_plots_vary_m',1:2,1:3, 1:3)
% generates the bottom row of plots in Fig. 5.2 in [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%% Set-up
addpath(genpath('../../'))

verbose = 1;                                                                % whether the user wants to print intermediate information to screen
halt_if_diverge = 1;                                                        % whether we expect monotone convergence (not guaranteed for FOM or RA on non-HPD matrices)
% With the option updated to have a bigger buffer, I think we should leave
% it on.

n = 5000;
s = 100;
loadstr = sprintf('data/%s_setup_%d_%d.mat',mfilename,n,s);
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        lin_sys_mod_setup(n,s)
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6417952/files/lin_sys_mod_setup_5000_100.mat?download=1'));
    end
end
lamA = data.lamA;
A = data.A;
B = data.B;
exact_sol = data.exact_sol;
condA = data.condA;
hermitian = data.hermitian;
clear data

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
mkdir(sprintf('%s/%s',teststr,output));

conv_plots = 0;
cycle_plots_vary_m = 0;
cycle_plots_vary_s = 0;
poly_plots = 0;

switch output
    case 'conv_plots'
        conv_plots = 1;
        
        cycle_length = 25;  % as in all the tests in the paper
        num_RHS = 10;
        max_restarts = [500 500 500;  % bounds don't hold but interesting
            500 500 500;                % all methods require the same effort-- interesting, because it shows that when eigs are clustered at zero, 
            50 50 50;                   % again, all methods are the same-- log dominates-- but the uni spaced values make convergence much quicker
            1000 1000 1000;  % interesting
            5000 5000 5000;  % interesting, but very difficult to run
            500 500 500; % interesting, bounds do not hold for FOM or RA
            500 500 500;
            5000 5000 5000];
        tol_err = 1e-10;
        
        if nargin == 1
            caseID = [1 2 4];
            modID = 1:3;
            ipID = 1:3;
        end
        
        % To make plots pretty
        minx = 0;
        miny = tol_err; maxy = 1;
        
    case 'cycle_plots_vary_m'
        cycle_plots_vary_m = 1;
        
        cycle_length = 10:5:50;
        num_RHS = 10;
        max_restarts = n/10;
        tol_err = 1e-6;
        
        if nargin == 1
            caseID = [1 2 4];
            modID = 1:3;
            ipID = 1:3;
        end
        
        % To make plots pretty
        minx = 0;
        miny = tol_err; maxy = 1;
        
    case 'cycle_plots_vary_s'
        cycle_plots_vary_s = 1;
        
        cycle_length = 25;
        num_RHS = 5:5:20;
        max_restarts = n/10;
        tol_err = 1e-6;
        
        if nargin == 1
            caseID = [1 2 4];
            modID = 1:3;
            ipID = 1:3;
        end
        
        % To make plots pretty
        minx = 0;
        miny = tol_err; maxy = 1;

    case 'poly_plots'
        poly_plots = 1;
        
        cycle_length = 3;
        num_RHS = 5;
        max_restarts = 1;
        tol_err = 0;
        
        if nargin == 1
            caseID = [1:3 8];
            modID = 1:3;
            ipID = 1:3;
        end
end
if nargin == 2
    modID = 1:3;
    ipID = 1:3;
elseif nargin == 3
    ipID = 1:3;
end
    
if ischar(caseID)
    caseID = str2double(caseID);                                            % fixes a problem with executing Matlab in an external command line
end
if ischar(ipID)
    ipID = str2double(ipID);                                                % fixes a problem with executing Matlab in an external command line
end
if ischar(modID)
    modID = str2double(modID);                                              % fixes a problem with executing Matlab in an external command line
end

ip_options = {'cl_full', 'gl', 'li', 'nb','hy'};
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
ip_markers = {'-o','-.*','--s'};
num_ips = length(ipID);

mod_options = {'none','harmonic', 'radau-arnoldi'};
mod_str = {'FOM','GMRES', 'RA'};
norm_options = {'A-fro','harmonic','radau'};
method_str = cell(3,num_ips);
% Think of a method as an inner product + a modification (in fact, this is
% precisely what the labels are)

% Plot appearance
linewidth = 1.5;
cmap = parula(3)*.75;
colors = cell(3,1);
colors{1} = cmap(1,:); colors{2} = cmap(2,:); colors{3} = cmap(3,:);        % different color for each inner product

for c = caseID
    if cycle_plots_vary_m || cycle_plots_vary_s
        max_cycles = zeros(length(modID),num_ips,max(cycle_length),...
            max(num_RHS));                                                  % set up matrix to store number of cycles per run
    end
    
    for m = cycle_length
        for s = num_RHS
            lmax = max(real(lamA{c})); lmin = min(real(lamA{c}));
            Theta = lmax*1.01*eye(s);                                       % prescribe solvent for RA
%             Theta = lmin*.9*eye(s);             % only works when m is large enough

            if poly_plots
                P = cell(2,num_ips);                                        % storage for polynomial function
                eigH = cell(2,num_ips);                                     % storage for eigenvalues of the block upper Hessenberg + its modification
                detPH = cell(2,num_ips);                                    % storage for det(P(z))
            end

            for mod = modID
                if conv_plots                                               % open plot window for fixed modification; then run over different inner products
                    clf; hold on;
                    maxx = 0;
                end
                
                for ip = ipID
                    %% Run BFOM+mod
                    fprintf('--------------------Case %d, %s-%s, m = %d, s = %d --------------------\n',...
                        c,ip_str{ip},mod_str{mod},m,s);
                    method_str{mod,ip} = ...
                        sprintf('%s-%s',ip_str{ip},mod_str{mod});           % store labels for legend
                    param = [];
                    param.exact = exact_sol{c}(:,1:s);
                    param.hermitian = hermitian(c);
                    param.inner_product = ip_options{ip};
                    
                    if ~strcmp(mod_options{mod},'none')                     % set modification parameters
                        param.mod = mod_options{mod};
                    end
                    param.Theta = Theta;                                    % set prescribed solvent for RA
                    
                    if conv_plots                                           % set special norm for measuring error
                        param.max_restarts = max_restarts(c,ip);
                        if hermitian(c) || (~hermitian(c) && strcmp(mod_options{mod},'harmonic'))
                            param.norm = norm_options{mod};
                            param.error_scale = block_norm(param.exact,param,A{c});
                        else
                            param.norm = 'fro';
                            param.error_scale = norm(param.exact,'fro');
                        end
                    else
                        param.max_restarts = max_restarts;
                        param.norm = 'fro';
                        param.error_scale = norm(param.exact,'fro');
                    end
                    
                    param.halt_if_diverge = halt_if_diverge;
                    param.restart_length = m;
                    param.tol_err = tol_err;
                    param.verbose = verbose;
                    
                    param = param_init_bfom(param);                         % set remaining parameters
                    
                    param = bfom(A{c},B(:,1:s),param);                      % execute that linear solver!
%                     switch param.flag
%                         case 1
%                             fprintf('Converged to desired tolerance.\n');
%                         case 2
%                             fprintf('Stagnated before reaching desired tolerance.\n');
%                         case 3
%                             fprintf('Maximum number of cycles reached before convergence.\n');
%                         case 4
%                             fprintf('Maximum number of quadrature nodes reached before convergence.\n')
%                         case 5
%                             fprintf('Method broke down-- check error to see if desired tolerance reached.\n')
%                     end
                    
                    if halt_if_diverge                                        % distinguish between failure to converge and exhausting cycle allowance
                        if param.flag == 1 || param.flag == 3
                            fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error));
                            max_cycles(mod,ip,m,s) = length(param.exact_error);
                        else
                            fprintf('%s: NaN\n\n',method_str{mod,ip});
                            max_cycles(mod,ip,m,s) = NaN;
                        end
                    else
                        fprintf('%s: %d\n\n',method_str{mod,ip},length(param.exact_error));
                        max_cycles(mod,ip,m,s) = length(param.exact_error);
                    end

                    %% Convergence plots
                    if conv_plots                                           % plot each curve
                        maxx = max(maxx,length(param.exact_error));
                        miny = min(miny,param.exact_error(end));
                        semilogy(0:length(param.exact_error),[1 param.exact_error],...
                            ip_markers{ip}(1:end-1),'MarkerSize',2,...
                            'Color',colors{ip},...
                            'LineWidth',linewidth);
                        savestr = sprintf('%s/%s/case%d_%s_%s_%d_%d_%d',...
                            teststr,output,c,ip_str{ip},mod_str{mod},n,m,s); % string for saving .mat file
                        save(savestr,'param')                               % save param struct in case you have to regenerate the plots
                    end
                    
                    %% Set up polynomial plots
                    if poly_plots
                        Is = eye(s);
                        if ~isfield(param,'M')
                            if ip == 2                                      % toggle different settings for global inner product
                                param.M = 0*param.em;
                            else
                                param.M = 0*param.Em;
                            end
                        end
                        if ip == 2
                            M = param.M*param.em';                          % modification matrix
                            C = interpmatpoly(param.Hshort + M,...
                                param.e1*param.beta);                       % solve for coefficients
                        else
                            M = param.M*param.Em';                          % modification matrix
                            C = interpmatpoly(param.Hshort + M,...
                                param.E1*param.Beta);                       % solve for coefficients
                        end
                        P{mod,ip} = @(z) Is-z*matpolyval(C,z,1);            % define residual polynomial
                        eigH{mod,ip} = sort(eig(param.Hshort + M));         % store Ritz values
                        detPH{mod,ip} = ...
                            detmatpolyval(P{mod,ip},eigH{mod,ip});          % store polynomial evaluated on Ritz values
                    end
                    fprintf('\n');
                end                
                %% Finish up convergence plots
                if conv_plots                                               % make the plot pretty
                    axis([minx maxx miny maxy])
                    axis square
                    xlabel('cycle index')
                    grid on
                    set(gca,'FontSize',18,'YScale','log',...
                        'YMinorGrid','off','XMinorGrid','off')
                    legend(method_str(mod,:),'Location','Best')
                    
                    % Save plots
                    savestr = sprintf('%s/%s/case%d_%s_%d_%d_%d',...
                            teststr,output,c,mod_str{mod},n,m,s);           % string for saving .fig and .eps files
                    savefig(gcf,savestr);                                   % save .fig file for references
                    saveas(gcf,savestr,'epsc');                             % save .eps file for paper
                    close(gcf);
                end
                fprintf('\n');
            end
            fprintf('\n');
        end
        fprintf('\n');
    end
    
%% Cycle plots
    if cycle_plots_vary_m || cycle_plots_vary_s                             % plot the results for the different inner products
        for mod = modID
            clf; hold on;                                                   % new plot for each modification
            switch output
                case 'cycle_plots_vary_m'
                   savestr = sprintf('%s/%s/case%d_%s_%d_m_%d',...
                        teststr,output,c,mod_str{mod},n,s);
                    x = cycle_length;
                case 'cycle_plots_vary_s'
                    savestr = sprintf('%s/%s/case%d_%s_%d_%d_s',...
                        teststr,output,c,mod_str{mod},n,m);
                    x = num_RHS;
            end
            for ip = ipID
                semilogy(x,squeeze(max_cycles(mod,ip,cycle_length,num_RHS)),...
                    ip_markers{ip},'MarkerSize',3,...
                    'Color',colors{ip},...
                    'LineWidth',linewidth);
            end
            xlabel('cycle length')
            grid on
            set(gca,'FontSize',18,'YScale','log',...
                'YMinorGrid','off','XMinorGrid','off')
            legend(method_str(mod,:),'Location','Best')

            save(savestr,'max_cycles')                                      % save .mat file in case you have to regenerate the plots
            savefig(gcf,savestr);                                           % save .fig file for reference
            saveas(gcf,savestr,'epsc');                                     % save .eps file for paper
            close(gcf);
        end
    end
    
%% Polynomial plots
    if poly_plots
        Lam = [];
        for mod = modID
            for ip = ipID
                Lam = [Lam eigH{mod,ip}'];                                  % make sure we include Ritz values
            end
        end
        Lam = unique(Lam);

        xmin = min([real(Lam) 0]); xmax = max(real(Lam));

        for mod = modID
            for ip = ipID                
                cla
                x = sort(unique([linspace(xmin,xmax,n), Lam]));             % make sure we include Ritz values
                plot(x,detmatpolyval(P{mod,ip},x),...                       % plot polynomial on x
                    eigH{mod,ip},detPH{mod,ip},'o',...                      % plot polynomial at Ritz values
                    'LineWidth',linewidth,...
                    'MarkerSize',2)
                legend(method_str{mod,ip});
                grid on
                set(gca,'XLim',[xmin xmax],'FontSize',18,...
                    'YMinorGrid','off','XMinorGrid','off')
                xlabel('x'); ylabel('{$\det P(x)$}','Interpreter','latex');
                
                % Save plots
                savestr = sprintf('%s/poly_plots/case%d_%s_%s',...
                    teststr,c,ip_str{ip},mod_str{mod});
                savefig(gcf,savestr);
                saveas(gcf,savestr,'epsc');
                close(gcf);
            end
        end
    end
end
end

%% Auxiliary functions
function detP = detmatpolyval(P,z)
% Evaluates determinant of the matrix polynomial P on a vector or matrix z
[I,J] = size(z);
detP = 0*z;
for i = 1:I
    for j = 1:J
        detP(i,j) = det(P(z(i,j)));
    end
end
end
