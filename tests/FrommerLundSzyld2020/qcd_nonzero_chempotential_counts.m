function [max_cycles, Nquad_total_count] = qcd_nonzero_chempotential_counts(caseID,system_size)
% QCD_NONZERO_CHEMPOTENTIAL_MAX_CYCLES returns filled out tensors of
% max_cycle and Nquad_total_count data from the specified caseID.
%
% With outputs from
% qcd_nonzero_chempotential_mod(3, 1:3, [2 5 10], 1:3, 'large'),
% this file generates the values populating Table 5.1 in
% [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%%
addpath(genpath('../../'))

ipID = 1:3;
ip_str = {'cl', 'gl', 'li', 'nb','hy'};
num_ips = length(ipID);

modID = 1:3;
mod_str = {'BFOMFOM','Harmonic','Radau'};
method_str = cell(3,num_ips);
cycle_lengths = [2 5 10];
ipID = 1:3;

switch system_size
    case 'small'
        n = 3072;
        s = 12;
    case 'big'
        n = 16*3072;
        s = 12;
end

teststr = 'results/qcd_nonzero_chempotential_mod';
for c = caseID
    max_cycles = zeros(length(modID),length(ipID),max(cycle_lengths));                           % storage for max_cycle plots
    Nquad_total_count = max_cycles;
    for m = cycle_lengths
        for mod = modID
            for ip = ipID
                method_str{mod,ip} = ...
                    sprintf('%s-%s',ip_str{ip},mod_str{mod});
                    loadstr = sprintf('%s/case%d_%s_%d_%d_%d.mat',...
                        teststr,c,method_str{mod,ip},n,m,s);
                if exist(loadstr,'file')
                    data = load(loadstr);
                    max_cycles(mod,ip,m) = length(data.exact_error);
                    Nquad_total_count(mod,ip,m) = sum(data.Nquad_per_cycle);
                end
            end
        end
    end
    savestr = sprintf('%s/case%d_%d',teststr,c,n);
    save(savestr,'max_cycles','Nquad_total_count');
end
end