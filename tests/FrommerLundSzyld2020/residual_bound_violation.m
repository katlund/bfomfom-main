function residual_bound_violation(mod_str)
% RESIDUAL_BOUND_VIOLATION(mod) looks for scenarios where the bound
% ||R_m(t)|| <= ||R_m(0)|| is violated and saves the associated Hessenberg
% matrix H and the t_bad for which the bounds are violated.  It also
% produces two plots: one of ||R_m(t)|| - ||R_m(0)|| scaled by ||R_m(0)||
% and one of the norm of the cospatial factor.
%
% This script can be used to generated the results in Fig. 4.1 of
% [FrommerLundSzyld2020].
%
% This file is part of the modified B(FOM)^2 code described in detail in
% [FrommerLundSzyld2020].

%%
if nargin == 0
    mod_str = 'harmonic';
end

global teststr
teststr = sprintf('results/%s/%s',mfilename,mod_str);
mkdir(teststr);

global tol_err
tol_err = 1e-6;                                                             % what we regard as 0 for root-finding

%% Wrapper
maxruns = 10000;
for runID = 1:maxruns
    [flag, data] = hunt_for_bad_apples(mod_str,runID);
    if flag
        savestr = sprintf('%s/run_%d',teststr,runID);
        save(savestr,'data')
        return
    end
end
end

%% Main function
function [flag, data] = hunt_for_bad_apples(mod_str,runID)
% [flag, data] = HUNT_FOR_BAD_APPLES(mod_str,runID) builds a residual
% function associated to a symmetric positive definite block tridiagonal
% matrix. Whether the example produces a residual that violates the bound
% is marked by flag; if so, the associated Hessenberg H and violating
% shifts t_bad are stored in the data struct.
global teststr
global tol_err
data = [];

% Set up a symmetric block tridiagonal matrix
cond = 100;                                                                 % condition number of the matrix
lmin = 0.1;                                                                 % smallest eigenvalue
s = 2;                                                                      % block size
m_max = 10;                                                                 % the matrix has m_max block rows and colums

H = zeros(s*m_max,s*m_max);
for i = 1:m_max-1                                                           % sub diagonal
    H(s*(i)+1:s*(i+1), s*(i-1)+1:s*i) = randspd(s);
end
H = H + H';                                                                 % add superdiagonal symmetrically 
for i = 1:m_max                                                             % add block diagonal
    H(s*(i-1)+1:s*i, s*(i-1)+1:s*i) = randspd(s);
end
c = sort(eig(H));

% Make H have lambda_min = lmin, lambda_max = cond*lmin
gamma = (c(s*m_max) - cond*c(1))/(cond-1);
alpha = lmin/(c(1)+gamma);
H = alpha*(gamma*speye(size(H))+H);

% Useful bits
Is = eye(s);                                                                % identity matrix of size s
m = m_max - 1;
Im = eye(m); Ims = eye(m*s);                                                % identity matrices of sizes m and ms, respectively
Em = kron(Im(:,end),Is); E1 = kron(Im(:,1),Is);                             % block unit vectors

switch mod_str
    case {'bfom', 'fom', 'none'}
        Xim = @(t) (H(1:s*m,1:s*m) + t*Ims)\E1;                             % (H_m + tI)\E_1
        Gm = @(t) Em'*Xim(t);                                               % cospatial factor
        normS_Rm = @(t) ...
            norm(-H(m*s+1:(m+1)*s,(m-1)*s+1:m*s)*Gm(t),'fro');              % ||R_m(t)||_S = ||-H_{m+1,m} G_m(t)||_F
        
        ritz_check = 1;                                                     % Ritz values are in the right-half plane by design
        
    case {'bgmres', 'gmres', 'har', 'harmonic'}
        Hnext = -H(m*s+1:(m+1)*s,(m-1)*s+1:m*s);
        M = (H(1:s*m,1:s*m)'\Em)*(Hnext'*Hnext);                            % BGMRES modification matrix
        
        Xim = @(t) (H(1:s*m,1:s*m) + M*Em' + t*Ims)\E1;                     % (H_m + tI)\E_1
        Gm = @(t) Em'*Xim(t);                                               % cospatial factor
        normS_Rm = @(t) norm([M; -Hnext]*Gm(t),'fro');                      % ||R_m(t)||_S = ||[M; -H_{m+1,m}] G_m(t)||_F
        
        ritz_check = sum(real(eig(H(1:s*m,1:s*m) + M*Em')) < tol_err) == 0; % checks whether Ritz values are in the right-half plane
end
tmax = 2;                                                                   % largest shift
Cm = @(t) Gm(0)\Gm(t);                                                      % polynomial cospatial factor

if ritz_check
    % Search for zeros of residual difference function
    Rm_diff = @(t) (normS_Rm(t) - normS_Rm(0))/normS_Rm(0);
    [flag, t_bad] = zero_search(Rm_diff,tmax);
else 
    % Then the matrix doesn't meet the requirements
    flag = false;
end

%% Plot
% Plot the curve R(t) and highlight its maximum as well as the points at
% which it crosses the x-axis (t_bad). I conjecture that the maximum
% corresponds to where the largest eigenvalue of H(t) first exceeds the
% largest eigenvalue of A
if flag
    t = linspace(0,tmax,5000);                                              % the interval on which we examine the residual function
    t = sort([t t_bad]);

    % Plot residual difference function, along with max and t_bad
    R = f_eval(Rm_diff,t);
    R_bad = f_eval(Rm_diff,t_bad);
    [maxR,indR] = max(R);

    clf;
    plot(t,R,'-',t(indR),maxR,'h',t_bad,R_bad,'pk',...
        'MarkerSize',2,'LineWidth',2); hold on;
    plot([0 t(end)],[0 0],'-k')                                             % x-axis
    set(gca,'FontSize',22)
    axis tight
    xlabel('t')
    legend('R(t)', 'R(t_{max})', 'R(t_{bad}) = 0')
    str = sprintf('$R(t) := \\frac{||R_{%d}(t)||_S }{||R_{%d}(0)||_S} - 1$',m,m);
    title(str,'Interpreter','latex');

    savestrR = sprintf('%s/residual_run_%d',teststr,runID);
    savefig(gcf,savestrR);
    saveas(gcf,savestrR,'epsc');


    % Plot the absolute value of determinant of the cospatial function,
    % along with max and t_bad
    abs_det_Cm = @(t) abs(det(Cm(t)));
    C = f_eval(abs_det_Cm,t);
    C_bad = f_eval(abs_det_Cm,t_bad);

    figure
    plot(t,C,'-',t(indR),abs_det_Cm(t(indR)),'h',t_bad,C_bad,'pk',...
        'MarkerSize',2,'LineWidth',2); hold on;
    plot([0 t(end)],[0 0],'-k')                                             % x-axis
    set(gca,'FontSize',22)
    axis tight
    xlabel('t')
    legend('C(t)', 'C(t_{max})', 'C(t_{bad})')
    str = sprintf('$C(t) := |\\det(C_%d(t))|$',m);
    title(str,'Interpreter','latex');

    savestrG = sprintf('%s/cospatial_run_%d',teststr,runID);
    savefig(gcf,savestrG);
    saveas(gcf,savestrG,'epsc');

    % Save outputs
    data.H = H;
    data.t_bad = t_bad;
end
end

%% Zero search and curve plotting
function [flag, t_bad] = zero_search(Rm_diff,tmax)
% [flag, t_bad] = ZERO_SEARCH(Rm_diff,tmax) searches for zeros of the
% function handle Rm_diff. These zeros correspond to bound failures.  But
% we also have to account for numerical errors, so we throw away t_bad that
% are too small or for which R(t_bad) is too small.
global tol_err
flag = 0;                                                                   % no violating t has been found
warning('off')                                                              % suppress warnings from fzero
t_bad = [];
for j = linspace(tol_err,tmax,25)
    t_bad = [t_bad fzero(Rm_diff,j)];
end
t_bad = unique(sort(t_bad(t_bad >= tol_err)));

% Compute Rm_diff at t_bad
if ~isempty(t_bad)
    R_bad = f_eval(Rm_diff,t_bad);
    if sum(abs(R_bad) <= tol_err)
        flag = 1;  % found a bad t!
    end
end
end

%% Auxiliary functions
function out = f_eval(f,x)
% out = F_EVAL(f,x) evaluates a function handle f on x, when f is not
% defined vector-wise
[m,n] = size(x);
out = zeros(m,n);
for i = 1:m
    for j = 1:n
        out(i,j) = f(x(i,j));
    end
end
end

function A = randspd(n)
% A = RANDSPD(n) returns a random n x n symmetric positive definite matrix
% A
A = randn(n);
[U,Sigma,~] = svd(A);
A = U*Sigma*U';
A = 0.5*(A + A');
end