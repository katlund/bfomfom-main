#!/bin/bash

#SBATCH --job-name exp_mod
#SBATCH --nodes 4
#SBATCH --ntasks 3
#SBATCH --cpus-per-task 1
#SBATCH --mem 100G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=48:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
for caseID in 1 2 3 4
do
  for modID in 1 2 3
  do
    matlab -nodisplay -nosplash -nodesktop -r "exponential_mod ${caseID} ${modID}" &
  done
done

wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
