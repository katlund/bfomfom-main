#!/bin/bash
#SBATCH --job-name vary_m
#SBATCH --nodes 12
#SBATCH --ntasks 18
#SBATCH --cpus-per-task 1
#SBATCH --mem 153600
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=24:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
module purge
module load matlab
for caseID in 1 2 3 4 5 6 7 8
do
  for ipID in 1 2 3
  do
    for cycle_length in 10 15 20 25 30 35 40 45 50
    do
      for num_RHS in 10
      do
        matlab -nodisplay -nosplash -nodesktop -r "lin_sys_mod_cycle_plots ${caseID} ${ipID} ${cycle_length} ${num_RHS}" &
      done
    done
  done
done
wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
