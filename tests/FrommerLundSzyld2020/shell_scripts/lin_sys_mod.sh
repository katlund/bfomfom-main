#!/bin/bash

#SBATCH --job-name lin_sys_mod
#SBATCH --nodes 1
#SBATCH --ntasks 3
#SBATCH --cpus-per-task 1
#SBATCH --mem 50G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=12:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
module purge
module load matlab

for mod_id in 1 2 3
do
   matlab -nodisplay -nosplash -nodesktop -r "lin_sys_mod conv_plots 4 ${mod_id}" &
done

wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
