#!/bin/bash

#SBATCH --job-name qcd
#SBATCH --nodes 1
#SBATCH --ntasks 3
#SBATCH --cpus-per-task 1
#SBATCH --mem 128G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=24:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
for caseID in 3
do
  for modID in 1 2 3
  do
    for cycle_lengths in 10
    do
      for ipID in 2
      do
        matlab -nodisplay -nosplash -nodesktop -r "qcd_nonzero_chempotential_mod ${caseID} ${modID} ${cycle_lengths} ${ipID} big" &
      done
    done
  done
done
wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
