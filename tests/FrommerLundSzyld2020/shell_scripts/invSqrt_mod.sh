#!/bin/bash

#SBATCH --job-name invSqrt_mod
#SBATCH --nodes 1
#SBATCH --ntasks 12
#SBATCH --cpus-per-task 1
#SBATCH --mem 120G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=12:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
module purge
module load matlab

for caseID in 1 4
do
  for modID in 1 2 3
  do
    for ipID in 1 3
    do
      matlab -nodisplay -nosplash -nodesktop -r "invSqrt_mod ${caseID} ${modID} ${ipID}" &
    done
  done
done
wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
echo "$dt"
