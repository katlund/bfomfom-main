#!/bin/bash

#SBATCH --job-name conv_plots
#SBATCH --nodes 1
#SBATCH --ntasks 8
#SBATCH --cpus-per-task 1
#SBATCH --mem 20960
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=04:00:00
echo STARTING AT 'date'
module purge
module load matlab
for text_str in "conv_plots"
do
  for case_id in 1 2 3 4 5 6 7 8
  do
    matlab -nodisplay -nosplash -nodesktop -r "lin_sys_mod ${text_str} ${case_id}" &
        done
      done
wait
echo FINISHED AT 'date'
