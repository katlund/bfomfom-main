#!/bin/bash

#SBATCH --job-name qcd
#SBATCH --nodes 1
#SBATCH --ntasks 2
#SBATCH --cpus-per-task 1
#SBATCH --mem 32G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=6:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
# 3 5
for caseID in .3 .5
do
  matlab -nodisplay -nosplash -nodesktop -r "qcd_nonzero_chempotential_exact_sol ${caseID}" &
done
wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
