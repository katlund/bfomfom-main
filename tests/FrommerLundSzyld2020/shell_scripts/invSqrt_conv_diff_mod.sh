#!/bin/bash

#SBATCH --job-name invSqrt_conv_diff_mod
#SBATCH --nodes 1
#SBATCH --ntasks 6
#SBATCH --cpus-per-task 1
#SBATCH --mem 100G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=24:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
for Nu in 2500
do
  for N in 100 350
  do
    for modID in 1 2 3
    do
      matlab -nodisplay -nosplash -nodesktop -r "invSqrt_conv_diff_mod ${Nu} ${N} ${modID}" &
    done
  done
done

wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
