#!/bin/bash

#SBATCH --job-name mat_func
#SBATCH --nodes 3
#SBATCH --ntasks 15
#SBATCH --cpus-per-task 1
#SBATCH --mem 50G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=16:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
#for case_id in 1 2
#do
#  matlab -nodisplay -nosplash -nodesktop -r "nonnorm_nondiag ${case_id}" &
#done

for case_id in 1 2
do
  for mod_id in 1 2 3
  do
    matlab -nodisplay -nosplash -nodesktop -r "lapl_2d_mod ${case_id} 1:3 ${mod_id}" &
  done
done

for case_id in 0 100 200
do
  for mod_id in 1 2 3
  do
    matlab -nodisplay -nosplash -nodesktop -r "conv_diff_mod ${case_id} 1:3 ${mod_id}" &
  done
done

wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
