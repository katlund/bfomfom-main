#!/bin/bash

#SBATCH --job-name qcd_dirac_mod
#SBATCH --nodes 3
#SBATCH --ntasks 3
#SBATCH --cpus-per-task 1
#SBATCH --mem 50G
#SBATCH --output=out.%j
#SBATCH --error=err.%j
#SBATCH --time=24:00:00
dt=`date '+%d/%m/%Y %H:%M:%S'`
module purge
module load matlab
for modID in 1 2 3
do
  matlab -nodisplay -nosplash -nodesktop -r "qcd_dirac_mod ${modID}" &
done

wait
dt=`date '+%d/%m/%Y %H:%M:%S'`
