% TENSOR_TEXP_NETWORK computes the tensor t-exponential of a random network
% with B(FOM)^2 and the harmonic Arnoldi method for matrix functions.
%
% I don't remember why I neglected this file-- I think I wanted to try
% modified B(FOM)^2, but quickly realized it's unnecessary.  Anyway, the
% paper [Lund2020] explicitly cites TENSOR_TEXP_NETWORK_V2, so I guess that
% means we should have an original sitting around to justify its name.
% Honestly, I think this file was written before I even knew what
% version-control was, so... there you have it.  The hidden, existential
% header-rant you didn't know you needed today.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%
% This file was written specifically for [Lund2020].

%% Set-up
addpath(genpath('../../'))

ipID = 1:2;                                                                 % which inner products to run
caseID = 2:3; % 1 = block-wise  % 2 = bcirc(D) % 3 = bcirc(A)               % block-wise does not work well

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
loadstr = 'data/tensor_texp_network_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        tensor_texp_network_exact_sol
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6420778/files/tensor_texp_network_exact_sol.mat?download=1'));
    end
end
bcircA = data.bcircA;
blockdiagD = data.blockdiagD;
FB = data.FB;
fDFB = data.fDFB;
unfoldB = data.unfoldB;
exact_sol_A = data.exact_sol_A;
n = data.n; p = data.p; s = data.s;
clear data

num_ips = length(ipID);
hermitian = false;
m = 15;
max_restarts = 200;
halt_if_diverge = true;
tol_err = 1e-12;
tol_quad = tol_err;
verbose = 1;

inner_product = {'cl_full', 'gl', 'li', 'nb','hy'};
IPname = {'cl', 'gl', 'li', 'nb', 'hy'};

markers = {'-','--','-.'; ':','--','-.'};
linewidth = [2, 3];
cmap = cool(6)*.75;
colors = cell(2,1);
colors{1} = cmap([1 3 5],:); colors{2} = cmap([2 4 6],:);

mod = {[], 'harmonic'};
modstr = {'','+har'};
methodstr = cell(2,num_ips);

for c = caseID
    figure
    hold on;
    for ip = ipID
        param = [];
        if c == 2
            param.exact = unfold(fDFB,n,s,p);
        elseif c == 3
            param.exact = unfold(exact_sol_A,n,s,p);
        end
        param.error_scale = norm(param.exact,'fro');
        param.function = 'exp';
        param.hermitian = hermitian;
        param.inner_product = inner_product{ip};
        param.max_restarts = max_restarts;
        param.halt_if_diverge = halt_if_diverge;
        param.norm = 'fro';
        param.restart_length = m;
        param.tol_err = tol_err;
        param.tol_quad = tol_quad;
        param.verbose = verbose;
        for met = 1:2
            methodstr{met,ip} = sprintf('%s%s',IPname{ip},modstr{met});
            fprintf('%s\n',methodstr{met,ip})
            if met ~= 1
                param.mod = mod{met};
            end
            param = param_init_bfomfom(param);
            if c == 2
                param = bfomfom(blockdiagD,unfold(FB,n,s,p),param);
                titlestr = 'D';
            elseif c == 3
                param = bfomfom(bcircA,unfoldB,param);
                titlestr = 'A';
            end
            semilogy(param.exact_error,markers{met,ip},...
                'LineWidth',linewidth(met),...
                'Color',colors{met}(ip,:));
        end
    end
    xlabel('cycle count')
    ylabel('relative error in {||\cdot||_F}')
    title(titlestr)
    set(gca,'FontSize',16,'YScale','log')
    grid off; grid on;
    legend(methodstr(:),'Location','Best')
    savestr = sprintf('%s/case%d_%d_%d_%d',teststr,c,n,m,s);
    savefig(gcf,savestr);
    saveas(gcf,savestr,'epsc');             
    close(gcf);
    fprintf('\n');
end