% TENSOR_TEXP_NETWORK_EXACT_SOL generates and saves exact solutions block
% circulant example.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%
% This file was written specifically for [Lund2020].

%%
addpath(genpath('../../../'))

f = @(A) expm(A);
n = 40;
p = n;
s = n;      % of course s = n; helps to have a different variable for 2nd dimension
A_density = 5e-3;

unfoldAA = sparse(n*p,s);
ind = 1:n;
for k = 1:p
    unfoldAA(ind,:) = logical(sprand(n,s,A_density));
    for i = 1:n
        unfoldAA(ind(i),i) = 0;
    end
    unfoldAA(ind,:) = unfoldAA(ind,:) + unfoldAA(ind,:)';
    ind = ind + n;
end
D = fft(fold(unfoldAA,n,s,p),[],3);

unfoldB = speye(n*p,s);
FB = fft(fold(full(unfoldB),n,s,p),[],3);

fDFB = zeros(n,s,p);
for k = 1:p
    fDFB(:,:,k) = f(squeeze(D(:,:,k)))*squeeze(FB(:,:,k));
end
exact_sol_D = ifft(fDFB,[],3);

%% Generate bcircA
ind = 1:n*p;
[ind1,ind2,entries] = find(unfoldAA(ind,:));
bcircA = sparse(ind1,ind2,entries,n*p,n*p);
for k = 2:p
    ind = wshift(1,ind,-n);
    [ind1,ind2,entries] = find(unfoldAA(ind,:));
    ind2 = ind2 + (k-1)*s;
    bcircA = bcircA + sparse(ind1,ind2,entries,n*p,n*p);
end
exact_sol_A = fold(expm(full(bcircA))*unfoldB,n,s,p);

%% Generate blockdiagD
[ind2, ind1] = meshgrid(1:n,1:n);
blockdiagD = sparse(ind1,ind2,D(:,:,1),n*p,n*p);
for k = 2:p
    ind1 = ind1 + n; ind2 = ind2 + n;
    blockdiagD = blockdiagD + sparse(ind1,ind2,D(:,:,k),n*p,n*p);
end
savestr = sprintf('data/%s',mfilename);
save(savestr,'n','s','p',...
    'bcircA','unfoldB','exact_sol_A',...
    'blockdiagD','FB','fDFB')

fprintf('density of bcirc(A): %d; density of bcirc(D): %d\n',...
    nnz(bcircA)/(n*p)^2,nnz(blockdiagD)/(n*p)^2)

%% Spy plots
teststr = sprintf('results/%s','tensor_texp_network');
mkdir(teststr)
spy(bcircA)
savestr = sprintf('%s/bcircA_n%d_p%d',teststr,n,p);
savefig(gcf,savestr);
saveas(gcf,savestr,'epsc');

spy(blockdiagD)
savestr = sprintf('%s/blockdiagD_n%d_p%d',teststr,n,p);
savefig(gcf,savestr);
saveas(gcf,savestr,'epsc');

spy(blockdiagD(1:3*n,1:3*n))
savestr = sprintf('%s/blockdiagD_n%d_p%d_zoom',teststr,n,p);
savefig(gcf,savestr);
saveas(gcf,savestr,'epsc');

clf; hold on;
grid on; view(40,15);
ind = 1:n;
cmap = cool(p)*.8;
for k = 1:p
    [I,J] = find(bcircA(ind,1:n));
    K = k + 0*I;
    plot3(I,K,J,'.','MarkerSize',10,'Color',cmap(k,:))
    ind = ind + n;
end
xlabel('i'); ylabel('k'); zlabel('j');
savestr = sprintf('%s/spyAA_n%d_p%d',teststr,n,p);
savefig(gcf,savestr);
saveas(gcf,savestr,'epsc');
