function tensor_texp_network_v2(m, max_restarts, ploton)
% TENSOR_TEXP_NETWORK_V2 computes the tensor t-exponential of a random
% network with B(FOM)^2.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%
% This file was written specifically for [Lund2020] and generates Figure 4
% (m = 5) and Figure 5 (m = 2) and Table 1 therein, along with runs for m =
% 10 and m = 15.

%% Set-up
addpath(genpath('../../'))

ipID = 1:2;                                                                 % which inner products to run
caseID = 1:2; % 1 = bcirc(D) % 1 = bcirc(A)

teststr = sprintf('results/%s',mfilename);
mkdir(teststr);
loadstr = 'data/tensor_texp_network_v2_exact_sol.mat';
if isfile(loadstr)
    data = load(loadstr);
else
    inputstr = sprintf('File %s is missing.\n Would you like to generate it locally [1]\n or download it from Zenodo.org [0]?\n', loadstr);
    decision = input(inputstr);
    if decision == 1
        fprintf('The missing .mat file will be generated locally.\n');
        tensor_texp_network_v2_exact_sol
        data = load(loadstr);
    elseif decision == 0
        fprintf('The missing .mat file will be downloaded from Zenodo.org.\n');
        data = load(websave(loadstr, 'https://zenodo.org/record/6420778/files/tensor_texp_network_v2_exact_sol.mat?download=1'));
    end
end
data = load(loadstr);
bcircA = data.bcircA;
blockdiagD = data.blockdiagD;
FB = data.FB;
fDFB = data.fDFB;
unfoldB = data.unfoldB;
exact_sol_A = data.exact_sol_A;
n = data.n; p = data.p; s = data.s;
clear data

if nargin == 0
    m = 5;
    max_restarts = 30;
    ploton = true;
elseif nargin == 1
    max_restarts = 30;
    ploton = true;
elseif nargin == 2
    ploton = true;
end

num_ips = length(ipID);
hermitian = false;
halt_if_diverge = true;
tol_err = 1e-12;
tol_quad = tol_err;
verbose = 0;

inner_product = {'cl_full', 'gl'};
IPname = {'classical', 'global'};

markers = {'-','--','-.'};
colors = cool(3)*.75;

methodstr = cell(1,num_ips);

for c = caseID
    clf
    hold on;
    for ip = ipID
        param = [];
        if c == 1
            param.exact = unfold(fDFB,n,s,p);
        elseif c == 2
            param.exact = unfold(exact_sol_A,n,s,p);
        end
        param.error_scale = norm(param.exact,'fro');
        param.function = 'exp';
        param.hermitian = hermitian;
        param.inner_product = inner_product{ip};
        param.max_restarts = max_restarts;
        param.Nquadmax = 10000;
        param.halt_if_diverge = halt_if_diverge;
        param.norm = 'fro';
        param.restart_length = m;
        param.tol_err = tol_err;
        param.tol_quad = tol_quad;
        param.verbose = verbose;
        
        methodstr{ip} = sprintf('%s',IPname{ip});
        param = param_init_bfomfom(param);
        if c == 1
            param = bfomfom(blockdiagD,unfold(FB,n,s,p),param);
            titlestr = 'D';
        elseif c == 2
            param = bfomfom(bcircA,unfoldB,param);
            titlestr = 'A';
        end
        if ploton
            semilogy(param.exact_error,markers{ip},...
                'LineWidth',2,...
                'Color',colors(ip,:));
        end
        
        fprintf('%s, m = %d: relative error = %d | number of cycles = %d\n',...
            methodstr{ip}, m, param.exact_error(end), length(param.exact_error))
        
    end
    if ploton
        xlabel('cycle count')
        ylabel('relative error in {||\cdot||_F}')
        title(titlestr)
        set(gca,'FontSize',16,'YScale','log')
        grid off; grid on;
        legend(methodstr(:),'Location','Best')
        savestr = sprintf('%s/case%d_%d_%d_%d',teststr,c,n,m,s);
        savefig(gcf,savestr);
        saveas(gcf,savestr,'epsc');             
        close(gcf);
    end
    fprintf('\n');
end
