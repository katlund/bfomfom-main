function out = format_test_output(out)
% out = FORMAT_TEST_OUTPUT(out) formats data in given struct and saves it
% to the folder specified by out.teststr.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017].

%%
dirstr = sprintf('results/%s',out.teststr);
mkdir(dirstr)
savestr = sprintf('%s.tex',out.savestr);
fID = fopen(savestr,'w');

out.headings = {'wall time','A products','# cycles',...
    'mean time','true error','approx error'};                               % used to format TeX style tables
fprintf(fID, '\t %s\t %s\t %s\t %s\t %s\t %s\n', out.headings{:});
lIP = length(out.inner_product);
cskip = 0;
for c = 1:lIP
    fprintf(fID, '%s\t %.4g\t %10.4g\t %10.4g\t %15.4g\t %4.4e\t %4.4e\n',...
        out.inner_product{c},out.data(c,:));
    if ~isempty(out.exact_error{c})
        if c < 5
            semilogy(out.exact_error{c},...
                'Color',out.cmap(c,:),...
                'LineWidth',2,...
                'LineStyle',out.inner_product_plot{c});
        else
            semilogy(out.exact_error{c},...
                'Color',out.cmap(c,:),...
                'LineStyle','-',...
                'LineWidth',1,...
                'MarkerSize',4,...
                'Marker',out.inner_product_plot{c});
        end
        hold on
    else
        out.inner_product_legend(c-cskip) = [];
        cskip = cskip + 1;
    end
end

xlabel('cycle index')
switch out.norm
    case 'A-fro'
        normstr = '{||\cdot||_{A-F}}';
    case 'fro'
        normstr = '{||\cdot||_F}';
    case 'max-col'
        normstr = 'maxiumum column ';
    case {2,'2'}
        normstr = '2-';
    case {1,'1'}
        normstr = '1-';
    case {inf,Inf,'inf','Inf'}
        normstr = '{\infty}-';
end
ystr = sprintf('relative error measured in %s',normstr);
ylabel(ystr)
legend(out.inner_product_legend,'Location','Best')
set(gca,'FontSize',16)
grid on
hold off
grid off; grid on

savefig(gcf,out.savestr);

% LaTeX tabular format for paper
fprintf(fID,'\\begin{table}[htb!]\n');
fprintf(fID,'\\centering\n');
fprintf(fID,'\\begin{tabular}{l|c|c|c|c}\n');
fprintf(fID,'& \\multicolumn{1}{c|}{\\thead{wall\\\\time (s)}} ');
fprintf(fID,'& \\multicolumn{1}{c|}{\\thead{number\\\\of cycles}} ');
fprintf(fID,'& \\multicolumn{1}{c|}{\\thead{true\\\\error ($\\normAF{\\cdot}$)}} ');
fprintf(fID,'& \\multicolumn{1}{c}{\\thead{average time\\\\per cycle (s)}} \\\\ \\hline\n');
for c = 1:lIP
    fprintf(fID,'%s\t & %.4g\t & %g\t & %.4e\t & %.4g \\\\ ',...
        out.inner_product{c},out.data(c,[1 3 5 4]));
    if c < lIP
        fprintf(fID,'\\hline\n');
    else
        fprintf(fID,'\n');
    end
end
fprintf(fID,'\\end{tabular}\n');
fprintf(fID,'\\caption{Test description \\label{tab:test}}\n');
fprintf(fID,'\\end{table}\n');
fclose(fID);
save(out.savestr,'out');
