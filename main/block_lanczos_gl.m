function param = block_lanczos_gl(A,B,param)
% param = BLOCK_LANCZOS_GL(A,B,param) computes the block Lanczos basis with
% respect to A, B, and the global inner product.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
m = param.restart_length;
[n,s] = size(B);
V = [];
H = [];
if param.first_cycle
    param.beta = IP(B);
    V(:,:,1) = B/param.beta;
else
    V(:,:,1) = B;
end

param.Acount = 0;
flag = false;
for j = 1:m
    if j == 1
        if isnumeric(A)
            W = A*V(:,:,1);
        else
            W = A(V(:,:,1));
        end
    else
        if isnumeric(A)
            W = A*V(:,:,j) - V(:,:,j-1)*H(j-1,j);
        else
            W = A(V(:,:,j)) - V(:,:,j-1)*H(j-1,j);
        end
    end
    param.Acount = param.Acount + 1;
    H(j,j) = IP(V(:,:,j),W);
    W = W - V(:,:,j)*H(j,j);
    H(j+1,j) = IP(W);
    if abs(H(j+1,j)) <= param.tol_zero
        m = j;
        param.breakdown = j;
        e1 = zeros(m,1); e1(1,1) = 1;
        param.e1 = e1;
        param.Hnext = H(end,end);
        param.Hshort = H(1:end-1,:);
        V = reshape(V,n,(m+1)*s);
        param.Vnext = V(:,end-s+1:end);
        param.Vshort = V(:,1:end-s);
        flag = true;
        break
    end
    if j < m
        H(j,j+1) = H(j+1,j)';
    end
    V(:,:,j+1) = W/H(j+1,j);
end

e1 = zeros(m,1); em = e1;
e1(1) = 1; em(end) = 1;
param.e1 = e1;
param.em = em;
param.Hnext = H(end,end);
param.Hshort = H(1:end-1,:);
if flag
    V = reshape(V,n,m*s);
    param.Vnext = [];
    param.Vshort = V;
else
    V = reshape(V,n,(m+1)*s);
    param.Vnext = V(:,end-s+1:end);
    param.Vshort = V(:,1:end-s);
    param.breakdown = m;
end
end

function out = IP(X,Y)
s = size(X,2);
% Global inner product.  Output is a scalar.
if nargin == 1
    out = norm(X,'fro')/sqrt(s);
else
    out = trace(X'*Y)/s;
end
end
