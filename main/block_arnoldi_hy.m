function param = block_arnoldi_hy(A,B,param)
% param = BLOCK_ARNOLDI_HY(A,B,param) computes a hybrid block Arnoldi basis
% with respect to A, B, and a hybrid inner product without deflation.  One
% must assume that exact deflation will not occur in order to use this
% algorithm; there are no fail-safes for breakdowns.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
s = size(B,2);
global q
q = param.q;                                                                % size of diagonal blocks

m = param.restart_length;
if param.first_cycle
    [V, param.Beta] = N(B);
else
    V = B;
end
H = [];

jnew = 1:s;
param.Acount = 0;
for j = 1:m
    if isnumeric(A)
        W = A*V(:,jnew);
    else
        W = A(V(:,jnew));
    end
    param.Acount = param.Acount + 1;
    for jj = 1:j
        jold = (jj-1)*s+1:jj*s;
        H(jold,jnew) = IP(V(:,jold),W);
        W = W - V(:,jold)*H(jold,jnew);
    end
    jold = jnew;
    jnew = jold(end)+1:jold(end)+s;
    [Q, H(jnew,jold)] = N(W);
    V = [V Q];                                                              % store new Hy basis vectors
end

Is = eye(s);
E1 = zeros(size(H,2),s); E1(1:s,1:s) = Is;
param.E1 = E1;

Em = zeros(size(H,2),s);
Em(end-s+1:end,end-s+1:end) = Is;
param.Em = Em;

param.svec = s*ones(1,m+1);
param.Hnext = H(end-s+1:end,end-s+1:end);
param.Hshort = H(1:end-s,:);
param.Hfull = H;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
param.breakdown = m;
end

function S = IP(X,Y)
% Hybrid inner product
global q;
s = size(X,2);
p = s/q;
S = zeros(s,s);
for i = 1:p
    S(q*(i-1)+1:q*i,q*(i-1)+1:q*i) = X(:,q*(i-1)+1:q*i)'*Y(:,q*(i-1)+1:q*i);
end
end

function [Q, R] = N(X)            % used in 1st block Krylov paper
R = sqrtm(IP(X,X));
Q = X/R;
end

% function [Q, R] = N(X)
% [Q, R] = qr(X,0);
% end
