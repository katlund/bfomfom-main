function param = bfom_gl_mod(A,B,param)
% param = BFOM_GL_MOD(A,B,param) computes the modified restarted global
% BFOM approximation to the linear systparam.em AX = B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
fprintf('%s\n',mfilename)
T = 0;
tic;
param.first_cycle = true;                                                   % store Beta
param = krylov_basis(A,B,param);
Acount = param.Acount;

s = size(B,2);
if ~isfield(param,'mod')
    param.M = 0*param.em;
else
    switch param.mod
        case 'harmonic'
            param.M = (param.Hshort'\param.em)*param.Hnext'*param.Hnext;
        case 'radau-lanczos'
            param.theta = max(diag(param.Theta));
            Imless = eye(param.breakdown-1);
            emless = Imless(:,end);
            dvec = (param.Hshort(1:end-1,1:end-1) - Imless*param.theta)\emless;
            param.M = param.em*(param.theta + ...
                param.Hshort(end,end-1)*dvec(end)'*param.Hshort(end,end-1)'+...
                - param.Hshort(end,end));
        case 'radau-arnoldi'
            param.theta = max(diag(param.Theta));
            Im = eye(param.restart_length);
            Gamma = -param.Hnext*param.em'*((param.Hshort - Im*param.theta)\param.em);
            param.M = (param.em/Gamma)*param.Hnext;
        case 'none'
            param.M = 0*param.em;
    end
end
Xim0 = ((param.Hshort + param.M*param.em')\param.e1)*param.beta;
param.Fm = 0;
Vstart = -param.Vnext*param.Hnext;
ind = 1:s;
for i = 1:param.breakdown
    param.Fm = param.Fm + param.Vshort(:,ind)*Xim0(i);
    Vstart = Vstart + param.Vshort(:,ind)*param.M(i);
    ind = ind + s;
end

T = T + toc;
if isfield(param,'exact')
    exact_flag = true;
    param.exact_error = block_norm(param.Fm - param.exact,param,A)/...
        param.error_scale;                                                  % compute exact error
    if param.exact_error < param.tol_err
        param.flag = 1;
        print_flag(param.flag);
        return
    end
else
    exact_flag = false;
end

param.approx_error = [];
param.flag = 0;                                                             % initialize convergence flag
for k = 2:param.max_restarts
    tic; T(k) = 0;
    param = krylov_basis(A,Vstart,param);
    Acount = Acount + param.Acount;
    if ~isfield(param,'mod')
        param.M = 0*param.em;
    else
        switch param.mod
            case 'harmonic'
                param.M = (param.Hshort'\param.em)*param.Hnext'*param.Hnext;
            case 'radau-lanczos'
                param.theta = max(diag(param.Theta));
                Imless = eye(param.breakdown-1);
                emless = Imless(:,end);
                dvec = (param.Hshort(1:end-1,1:end-1) - Imless*param.theta)\emless;
                param.M = param.em*(param.theta + ...
                    param.Hshort(end,end-1)*dvec(end)'*param.Hshort(end,end-1)'+...
                    - param.Hshort(end,end));
            case 'radau-arnoldi'
                param.theta = max(diag(param.Theta));
                Im = eye(param.restart_length);
                Gamma = -param.Hnext*param.em'*((param.Hshort - Im*param.theta)\param.em);
                param.M = (param.em/Gamma)*param.Hnext;
            case 'none'
                param.M = 0*param.em;
        end
    end
    Xim0 = ((param.Hshort + param.M*param.em')\param.e1)*param.beta*Xim0(end);
    err_apx = 0;
    Vstart = -param.Vnext*param.Hnext;
    ind = 1:s;
    for i = 1:param.breakdown
        err_apx = err_apx + param.Vshort(:,ind)*Xim0(i);
        Vstart = Vstart + param.Vshort(:,ind)*param.M(i);
        ind = ind + s;
    end

    param.approx_error(k-1) = block_norm(err_apx,param,A)/...
        param.error_scale;                                                  % store approximate error from previous cycle

    T(k) = T(k) + toc;
    switch param.conv_check
        case 'exact'
            err_check = param.exact_error(k-1);
            if k > 2
                err_check_past = param.exact_error(1);
            end
        case 'approx'
            err_check = param.approx_error(k-1);
            if k > 2
                err_check_past = param.approx_error(1);
            end
    end
    if param.verbose
        fprintf('     %s: cycle %g, %s error = %1.4g\n',...
            mfilename,k-1,param.conv_check,err_check);
    end
    tic;

    if err_check < param.tol_err                                            % check for convergence
        param.flag = 1;
        break
    elseif k > 2 && param.halt_if_diverge                                     % check for stagnation
        if err_check >= err_check_past

            T(k) = T(k) + toc;
            if param.verbose
                fprintf('     %s: stagnation at cycle %g\n',mfilename,k-1);
            end
            tic;

            param.flag = 2;                                                 % stagnation
            break
        end
    end
    param.Fm = param.Fm + err_apx;                                          % update solution approximation

    T(k) = T(k) + toc;
    if exact_flag
        param.exact_error(k) = block_norm(param.Fm - param.exact,param,A)/...
            param.error_scale;                                              % compute exact error
    end
    tic;
    if param.breakdown < param.restart_length
        param.flag = 5;
        break
    end
end
if param.max_restarts == 1                                                  % when max_restarts = 1
    k = 1;
end
param.Acount = Acount/k;

T(k) = T(k) + toc;
param.time = T;

if k == param.max_restarts && param.flag == 0
    param.flag = 3;                                                         % maximum number of cycles reached
end

print_flag(param.flag)
end

function print_flag(flag)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('Only one cycle, huh?\n')
        else
            error('Flag unchanged.  Check code for bugs.\n')
        end
    case 1
        fprintf('Method converged to desired tolerance.\n')
    case 2
        fprintf('Method stagnated before reaching desired tolerance.\n')
    case 3
        fprintf('Maximum number of cycles reached before convergence.\n')
    case 5
        fprintf('Method broke down-- check error to see if desired tolerance reached.\n')
end
end
