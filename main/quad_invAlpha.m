function [w, t] = quad_invAlpha(N,alpha)
% [w,t] = QUAD_INVALPHA(N,alpha) computes the weights and nodes
% corresponding to the Gauss-Jacobi quadrature rule with N nodes to
% approximate the integral form of z^{-alpha}, as described in [A. Frommer,
% S. Guettel, and M. Schweitzer: Efficient and stable Arnoldi restarts for
% matrix functions based on quadrature, SIAM J. Matrix Anal. Appl.,
% 35:661-683, 2014].  This file has also been adapted from funm_quad, the
% body of code associated to the same paper.
%
% chebfun is required for this function.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
global delta
if nargin == 1                                                              % default is invSqrt
    alpha = .5;
end

if alpha == .5
    w = pi/N*ones(1,N);                                                     % use Marcel Schweitzer's short-cut for computing weights and nodes
    x = zeros(1,N);
    for i = 1:N
        x(i) = cos((2*i-1)/(2*N) * pi);
    end
else                                                                        % use chebfun
    addpath('../../chebfun')
    [x,w] = jacpts(N,-alpha,alpha-1,[-1,1]);
    w = w';
end


C = 2 * sin((1-alpha)*pi)*delta^(1-alpha)/pi;
w = C*w;                                                                    % include coefficient in weights

w = w./(1+x);                                                               % due to change of variables and to allow for generality in bfomfom scripts
[x, ind] = sort(x);
w = w(ind);
t = delta*(1-x)./(1+x);                                                     % change of variables


end
