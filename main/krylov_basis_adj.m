function out = krylov_basis_adj(param,X)
% out = KRYLOV_BASIS(param,X) returns the matrix representation of the
% adjoint of a block Krylov basis.  For the classical, loop-interchange,
% and hybrid inner products, the routine is identical to taking the adjoint
% of the basis.  For the global or user-defined inner products, a special
% routine is required.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
switch param.inner_product
    case {'cl_full', 'cl_defl'}
        out = param.Vshort';
    case 'gl'
        [n, ms] = size(param.Vshort);
        s = ms/param.restart_length;
        if nargin == 2
            V = param.Vshort'*X;
            out = zeros(param.restart_length,1);
            for j = 1:param.restart_length
                indj = (j-1)*s+1:j*s;
                out(j) = trace(V(indj,1:s));
            end
        else
            q = floor(n/s);
            I = eye(n);
            r = n - q*s;
            if r > 0
                I = [I zeros(n,s-r)];
                q = q+1;
            end
            V = param.Vshort'*I;
            out = param.Vshort';  % pre-set for size
            for i = 1:q
                for j = 1:param.restart_length
                    indi = (i-1)*s+1:i*s;
                    indj = (j-1)*s+1:j*s;
                    out(indj,indi) = trace(V(indj,indi))*eye(s);
                end
            end
        end
    case 'li'
        out = param.Vshort';
    case 'hy'
        out = param.Vshort';
    case 'ud'
        n = size(param.Vshort,1);
        s = size(param.Beta,1);
        I = eye(n);
        out = zeros(size(param.Vshort'));
        q = floor(n/s);
        for i = 1:q
            for j = 1:param.restart_length
                indi = (i-1)*s+1:i*s;
                indj = (j-1)*s+1:j*s;
                out(indj,indi) = IP(param.Vshort(:,indj),I(:,indi));
            end
        end
        r = n - q*s;
        if r > 0
            I = [I(:,end-r+1:end) zeros(n,s-r)];
            for j = 1:param.restart_length
                indj = (j-1)*s+1:j*s;
                [~,S] = defl(IP(indj,indi));
                out(indj,end-r+1:end) = S;
            end
        end
end

end

function S = IP(X,Y)
% user-defined inner product
end

function S = defl(S)
% user-defined delfation routine that returns S of a smaller dimension
% corresponding to its rank
end
