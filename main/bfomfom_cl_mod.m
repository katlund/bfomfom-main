function param = bfomfom_cl_mod(A,B,param)
% param = BFOMFOM_CL_MOD(A,B,param) computes the modified restarted
% classical B(FOM)^2 approximation to the matrix function product f(A)B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
mfilename = sprintf('bfomfom_%s_mod',param.inner_product);
fprintf('%s\n',mfilename)
global delta;
global T; T = 0;                                                            % pre-set walltime
tic;
param.first_cycle = true;                                                   % store Beta
param = krylov_basis(A,B,param);
Acount = param.Acount;

% Extract quantities needed for cospatial function. We have to break
% dependence on param; otherwise the in-line functions will store all
% previous param, which includes all previous basis vectors.
Beta = param.Beta;
E1 = param.E1;
Em = param.Em;
Hnext = param.Hnext;
Hshort = param.Hshort;

s = param.svec(end);
if ~isfield(param,'mod')
    M = 0*Em;
else
    switch param.mod
        case 'harmonic'
            M = (Hshort'\Em)*(Hnext'*Hnext);
        case 'radau-lanczos'
            Imless = eye(param.restart_length-1);
            Emless = kron(Imless,eye(s)); Emless = Emless(:,end-s+1:end);
            Dvec = (Hshort(1:end-s,1:end-s) - kron(Imless,param.Theta))\Emless;
            Hless = Hshort(end-s+1:end,end-2*s+1:end-s);
            M = Em*(param.Theta + Hless*Dvec(end-s+1:end,:)'*Hless'...
                - Hshort(end-s+1:end,end-s+1:end));
        case 'radau-arnoldi'
            Im = eye(param.restart_length);
            Gamma = -Hnext*Em'*((Hshort - kron(Im,param.Theta))\Em);
            M = (Em/Gamma)*Hnext;
        case 'none'                                                         % mainly for debugging purposes
            M = 0*Em;
    end
end

Xim = @(t) ((Hshort + t*eye(size(Hshort)) + M*Em')\E1)*Beta;
if strcmp(param.function,'exp')
    param.ritz = eig(Hshort + M*Em');                     % store Ritz values
end

switch param.function                                                       % compute initial approximation
    case 'invSqrt'
        delta = 1;
        param.alpha = .5;
        param.Fm = param.Vshort*(sqrtm(Hshort + M*Em')\E1)*Beta;
        param.quad = @(N,param) quad_invAlpha(N,.5);

    case 'invAlpha'
        delta = 1;
        param.Fm = param.Vshort*((Hshort + M*Em')^param.alpha\E1)*Beta;
        param.quad = @(N,param) quad_invAlpha(N,param.alpha);

    case 'logShiftInv'
        param.Fm = param.Vshort*...
            (logm(Hshort + M*Em' + eye(size(Hshort))))*((Hshort + M*Em')\E1)*Beta;
        param.quad = @(N,param) quad_logShiftInv(N);

    case 'exp'
        param.Fm = param.Vshort*expm(Hshort + M*Em')*E1*Beta;
        param.quad = @(N,param) quad_exp(N,param);

    case 'user'
        param.Fm = param.Vshort*userf(Hshort)*E1*Beta;
        param.quad = @(N,param) quad_userf(N,param);
end

T = T + toc;
if isfield(param,'exact')
    exact_flag = true;
    param.exact_error = block_norm(param.Fm - param.exact,param,A)/...
        param.error_scale;                                                  % compute exact error
    if param.exact_error < param.tol_err
        param.flag = 1;
        print_flag(param.flag);
        return
    end
else
    exact_flag = false;                                                     % store flag to save some time
end

param.Nquad_per_cycle = [];                                                 % pre-set storage for quadrature node count
param.approx_error = [];                                                    % pre-set storage for approximate error
param.flag = 0;                                                             % initialize convergence flag
param.condH = [];
for k = 2:param.max_restarts
    param.condH(k-1) = cond(Hshort + M*Em');
    T(k) = 0; tic;
    Gm = @(t) Em'*Xim(t);
    param = krylov_basis(A,[param.Vshort param.Vnext]*[M; -Hnext],param);
    Acount = Acount + param.Acount;

    % Extract quantities needed for cospatial function update
    Beta = param.Beta;
    E1 = param.E1;
    Em = param.Em;
    Hnext = param.Hnext;
    Hshort = param.Hshort;

    s = param.svec(end);
    if ~isfield(param,'mod')
        M = 0*Em;
    else
        switch param.mod
            case 'harmonic'
                M = (Hshort'\Em)*(Hnext'*Hnext);
            case 'radau-lanczos'
                Imless = eye(param.restart_length-1);
                Emless = kron(Imless,eye(s)); Emless = Emless(:,end-s+1:end);
                Dvec = (Hshort(1:end-s,1:end-s) - kron(Imless,param.Theta))\Emless;

                Hless = Hshort(end-s+1:end,end-2*s+1:end-s);
                M = Em*(param.Theta + Hless*Dvec(end-s+1:end,:)'*Hless'...
                    - Hshort(end-s+1:end,end-s+1:end));
            case 'radau-arnoldi'
                Im = eye(param.restart_length);
                Gamma = -Hnext*Em'*((Hshort - kron(Im,param.Theta))\Em);
                M = (Em/Gamma)*Hnext;
        end
    end

    Xim = @(t) ((Hshort + t*eye(size(Hshort)) + M*Em')\E1)*Beta*Gm(t);
    if strcmp(param.function,'exp')
        param.ritz = [param.ritz; eig(Hshort + M*Em')];                                   % store Ritz values
    end

    param = mat_err_func_mod(param,Xim,k);                                  % error function and approximation
    if param.flag == 4
        param.Nquad_per_cycle(k-1) = param.Nquadmax;
        break
    end
    err_apx = param.Vshort*param.Delta;                                     % differs from bfomfom_cl
    param.Nquad_per_cycle(k-1) = param.Nquad2;

    param.approx_error(k-1) = block_norm(err_apx,param,A)/...
        param.error_scale;                                                  % store approximate error from previous cycle

    T(k) = T(k) + toc;
    switch param.conv_check
        case 'exact'
            err_check = param.exact_error(k-1);
            if k > 2
                err_check_past = param.exact_error(1);
            end
        case 'approx'
            err_check = param.approx_error(k-1);
            if k > 2
                err_check_past = param.approx_error(1);
            end
    end
    if param.verbose
        fprintf('     %s: cycle %g, %s error = %1.4g\n',...
            mfilename,k-1,param.conv_check,err_check);
    end
    tic;

    if err_check < param.tol_err                                            % check for convergence
        param.flag = 1;
        break
    elseif k > 2 && param.halt_if_diverge                                     % check for stagnation
        if err_check >= err_check_past

            T(k) = T(k) + toc;
            if param.verbose
                fprintf('     %s: stagnation at cycle %g\n',mfilename,k-1);
            end
            tic;

            param.flag = 2;                                                 % stagnation
            break
        end
    end
    param.Fm = param.Fm + err_apx;                                          % update solution approximation

    T(k) = T(k) + toc;
    if exact_flag
        param.exact_error(k) = block_norm(param.Fm - param.exact,param,A)/...
            param.error_scale;
    end
    tic;

    if param.reduce_Nquad                                                   % reduce number of quadrature nodes
        if ~param.refined && param.Nquad1 > 2
            param.Nquad2 = param.Nquad1;
            param.Nquad1 = round(param.Nquad1/sqrt(2));
            if mod(param.Nquad1,2) == 1
                param.Nquad1 = param.Nquad1-1;
            end
        end
    end
end
param.condH(end+1) = cond(Hshort + M*Em');
if param.max_restarts == 1                                                  % when max_restarts = 1
    k = 1;
end
param.Acount = Acount/k;

T(k) = T(k) + toc;
param.time = T;

if k == param.max_restarts && param.flag == 0
    param.flag = 3;                                                         % maximum number of cycles reached
end

print_flag(param.flag);
end

%% Auxiliary functions
function print_flag(flag)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('Only one cycle, huh?\n')
        else
            error('Flag unchanged.  Check code for bugs.\n')
        end
    case 1
        fprintf('Method converged to desired tolerance.\n')
    case 2
        fprintf('Method stagnated before reaching desired tolerance.\n')
    case 3
        fprintf('Maximum number of cycles reached before convergence.\n')
    case 4
        fprintf('Maximum number of quadrature nodes reached before convergence.\n')
end
end
