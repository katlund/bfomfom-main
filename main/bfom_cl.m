function param = bfom_cl(A,B,param)
% param = BFOM_CL(A,B,param) computes the restarted classical BFOM
% approximation to the linear system AX = B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
mfilename = sprintf('bfom_%s',param.inner_product);
fprintf('%s\n',mfilename)
T = 0;
tic;
param.first_cycle = true;                                                   % store Beta
m = param.restart_length;
for j = 1:m
    param.restart_length = j;
    param = krylov_basis(A,B,param);
    Acount = param.Acount;
    Xi = param.Hshort\param.E1;
    param.continue_basis = true;
end
param.Fm = param.Vshort*Xi*param.Beta;
param.Cm = -param.Hnext*...
    Xi(end-param.svec(end-1)+1:end,end-param.svec(end-1)+1:end)*...
    param.Beta;                                                             % first cospatial factor

T = T + toc;
if isfield(param,'exact')
    exact_flag = true;
    param.exact_error = block_norm(param.Fm - param.exact,param,A)/...
        param.error_scale;                                                  % compute exact error
    if param.exact_error < param.tol_err
        param.flag = 1;
        print_flag(param.flag);
        return
    end
else
    exact_flag = false;                                                     % store flag to save some time
end
param.approx_error = [];                                                    % pre-set storage for approximate error
param.first_cycle = false;                                                  % for subsequent cycles, do not redefine Beta
param.flag = 0;                                                             % initialize convergence flag
for k = 2:param.max_restarts
    tic; T(k) = 0;
    param = krylov_basis(A,param.Vnext,param);
    Acount = Acount + param.Acount;
    Xi = param.Hshort\param.E1;                                             % compute approximate error from previous cycle
    Delta = Xi*param.Cm;
    err_apx = param.Vshort*Delta;
    param.approx_error(k-1) = block_norm(err_apx,param,A);                  % store approximate error from previous cycles

    T(k) = T(k) + toc;
    switch param.conv_check
        case 'exact'
            err_check = param.exact_error(k-1);
            if k > 2
                err_check_past = param.exact_error(k-2);
            end
        case 'approx'
            err_check = param.approx_error(k-1);
            if k > 2
                err_check_past = param.approx_error(k-2);
            end
    end
    if param.verbose
        fprintf('     %s: cycle %g, %s error = %1.4g\n',...
            mfilename,k-1,param.conv_check,err_check);
    end
    tic;

    if err_check < param.tol_err                                            % check for convergence
        param.flag = 1;
        break
    elseif k > 2 && param.halt_if_diverge                                     % check for stagnation
        if err_check >= err_check_past

            T(k) = T(k) + toc;
            if param.verbose
                fprintf('     %s: stagnation at cycle %g\n',mfilename,k-1);
            end
            tic;

            param.flag = 2;                                                 % stagnation
            break
        end
    end
    param.Fm = param.Fm + err_apx;                                          % update solution

    T(k) = T(k) + toc;
    if exact_flag
        param.exact_error(k) = block_norm(param.Fm - param.exact,param,A)/...
            param.error_scale;
    end
    tic;

    param.Cm = -param.Hnext*...
        Xi(end-param.svec(end-1)+1:end,end-param.svec(end-1)+1:end)*param.Cm;    % next cospatial factor
end
if param.max_restarts == 1                                                  % when max_restarts = 1
    k = 1;
end
param.Acount = Acount/k;

T(k) = T(k) + toc;
param.time = T;

if k == param.max_restarts && param.flag == 0
    param.flag = 3;                                                         % maximum number of cycles reached
end

print_flag(param.flag);
end

function print_flag(flag)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('Only one cycle, huh?\n')
        else
            error('Flag unchanged.  Check code for bugs.\n')
        end
    case 1
        fprintf('Method converged to desired tolerance.\n')
    case 2
        fprintf('Method stagnated before reaching desired tolerance.\n')
    case 3
        fprintf('Maximum number of cycles reached before convergence.\n')
end
end
