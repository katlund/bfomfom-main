function [w, t] = quad_logShiftInv(N)
% [w,t] = QUAD_LOGSHIFTINV computes the weights and nodes corresponding to
% the Gauss-Legendre quadrature rule with N nodes for the integral form of
% log(1+z)/z as described in [A. Frommer, S. Guettel, and M. Schweitzer:
% Efficient and stable Arnoldi restarts for matrix functions based on
% quadrature, SIAM J. Matrix Anal. Appl., 35:661-683, 2014].  This file has
% also been adapted from funm_quad, the body of code associated to the same
% paper.
%
% chebfun is required for this function.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
if N < 1000
% From funm_quad. Computes nodes and weights by solving a tridiagonal
% eigenproblem.  Faster when N < 1000.
    b_gauss = .5./sqrt(1-(2*(1:N-1)).^(-2));
    T_gauss = diag(b_gauss,1) + diag(b_gauss,-1);
    [V_gauss,D_gauss] = eig(T_gauss);
    x = diag(D_gauss); [x,ind] = sort(x);
    w = (2*V_gauss(1,ind).^2)';
else
% Chebfun method. Faster when N > 1000
    addpath('../../chebfun')
    [x,w] = legpts(N,[-1,1]);
    w = w';
    [x, ind] = sort(x);
    w = w(ind);
end
w = w./(1-x);                                                               % due to change of variable formula (allows for generality within bfomfom scripts)
t = 2./(1-x);                                                               % change of variables
end
