function param = block_lanczos(A,B,param)
% param = BLOCK_LANCZOS(A,B,param) computes the block orthogonal Lanczos
% basis for K_m(A,B), with respect to given block inner product and scaling
% quotient.  The following methods are hard-coded:
%   'cl_full' - the classical method without deflation
%   'cl_defl' - the classical method with deflation
%   'gl' - the global method
%   'li' - the loop-interchange method with deflation
%   'hy' - a hybrid method without deflation
%   'ud' - a user-defined method
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
if ~isfield(param,'first_cycle')
    param.first_cycle = true;
end
switch param.inner_product
    case 'cl_full'
        param = block_lanczos_cl_full(A,B,param);
    case 'cl_defl'
        param = block_lanczos_cl_defl(A,B,param);
    case 'gl'
        param = block_lanczos_gl(A,B,param);
    case 'li'
        param = block_lanczos_li(A,B,param);
    case 'hy'
        param = block_lanczos_hy(A,B,param);
    case 'ud'
        param = block_arnoldi_ud(A,B,param);
end
end
