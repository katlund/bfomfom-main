function N = block_norm(X,param,A)
% N = BLOCK_NORM(X,param,A) computes a scalar norm for the block vector X.
%
% This file is a part of the B(FOM)^2 code described in detail in
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
% Defaults
if nargin == 1
    param.norm = 'fro';
elseif nargin == 2
    if ~isfield(param,'norm')
        param.norm = 'fro';
    end
elseif nargin == 3
    if ~strcmp(param.norm,'radau')
        if isnumeric(A)
            A = @(X) A*X;
        end
    end
end

switch param.norm
    case 'A-fro'                                                            % A-weighted Frobenius norm
        N = sqrt(real(trace(X'*(A(X)) ) ) );
    case 'fro'                                                              % Frobenius norm; default
        N = norm(X,'fro');
    case 'radau'
        % This doesn't work with non-numeric A unless the user also
        % provides a way to estimate solutions to shifted linear systems of
        % the A-operator...
        N = sqrt(real(trace(X'*...
            (A*( (param.Theta(1)*speye(size(A)) - A) \ X) ) ) ) );
    case 'harmonic'
        N = norm(A(X),'fro');
    case 'max-col'                                                          % max over the 2-norms of each column
        s = size(X,2);
        N = zeros(1,s);
        for i = 1:s
            N(i) = norm(X(:,i),2);
        end
        N = max(N);
    case {2,'2'}                                                            % matrix 2-norm
        N = norm(X,2);
    case {1,'1'}                                                            % matrix 1-norm
        N = norm(X,1);
    case {inf,Inf,'inf','Inf'}                                              % matrix inf-norm
        N = norm(X,Inf);
end
