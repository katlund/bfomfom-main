function C = interpmatpoly(H,V,f)
% C = INTERPMATPOLY(H,V,f) returns the coefficients associated to the
% interpolating matrix polynomial Q such that Q(H) \circ V = f(H)V. f may
% be a function handle or a string for a special built-in function:
%   'inv' - computes H\V
%   'sqrt' - sqrtm
%   'log' - logm
%   'exp' - expm
% The default for f is 'inv'.
%
% One may also feed in a precomputed value for f(H)*V in the field for f.
%
% The dimensions of H and V should be compatible, i.e., H is size ms x ms
% and V is ms x s. Then C is a block vector of size ms x s.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
[ms, s] = size(V);
m = ms/s;

if nargin == 2
    f = 'inv';
end

if isa(f,'function_handle')
    fHV = f(H)*V;
elseif isa(f,'double')      % when f(H)*V is pre-computed
    fHV = f;
else
    switch f
        case 'inv'
            fHV = H\V;
        case 'sqrt'
            fHV = sqrtm(H)*V;
        case 'log'
            fHV = logm(H)*V;
        case 'exp'
            fHV = expm(H)*V;
    end
end

VV = zeros(ms,ms);
ind = 1:s;
VV(:,ind) = V;
for j = 2:m
    ind = ind + s;
    VV(:,ind) = H*VV(:,ind-s);
end

C = VV\fHV;
end
