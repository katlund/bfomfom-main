function param = bfom_cl_mod(A,B,param)
% param = BFOM_CL_MOD(A,B,param) computes the modified restarted classical
% BFOM approximation to the linear system AX = B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
mfilename = sprintf('bfom_%s_mod',param.inner_product);
fprintf('%s\n',mfilename)
T = 0;
tic;
param.first_cycle = true;                                                   % store Beta
param = krylov_basis(A,B,param);
Acount = param.Acount;
if ~isfield(param,'mod')
    param.M = 0*param.Em;
else
    switch param.mod
        case 'harmonic'
            param.M = (param.Hshort'\param.Em)*param.Hnext'*param.Hnext;
        case 'radau-lanczos'
            s = param.svec(end);
            Imless = eye(param.restart_length-1);
            Emless = kron(Imless,eye(s)); Emless = Emless(:,end-s+1:end);
            Dvec = (param.Hshort(1:end-s,1:end-s) - kron(Imless,param.Theta))\Emless;
            Hless = param.Hshort(end-s+1:end,end-2*s+1:end-s);
            param.M = param.Em*(param.Theta + Hless*Dvec(end-s+1:end,:)'*Hless'...
                - param.Hshort(end-s+1:end,end-s+1:end));
        case 'radau-arnoldi'
            Im = eye(param.restart_length);
            Gamma = -param.Hnext*param.Em'*((param.Hshort - kron(Im,param.Theta))\param.Em);
            param.M = (param.Em/Gamma)*param.Hnext;
        case 'none'                                                         % mainly for debugging purposes
            param.M = 0*param.Em;
    end
end
Xim0 = ((param.Hshort + param.M*param.Em')\param.E1)*param.Beta;
param.Fm = param.Vshort*Xim0;

T = T + toc;
if isfield(param,'exact')
    exact_flag = true;
    param.exact_error = block_norm(param.Fm - param.exact,param,A)/...
        param.error_scale;                                                  % compute exact error
    if param.exact_error < param.tol_err
        param.flag = 1;
        print_flag(param.flag);
        return
    end
else
    exact_flag = false;                                                     % store flag to save some time
end

param.approx_error = [];                                                    % pre-set storage for approximate error
param.flag = 0;                                                             % initialize convergence flag
for k = 2:param.max_restarts
    T(k) = 0; tic;
    param = krylov_basis(A,[param.Vshort param.Vnext]*[param.M; -param.Hnext],param);
    Acount = Acount + param.Acount;
    s = param.svec(end);
    if ~isfield(param,'mod')
        param.M = 0*param.Em;
    else
        switch param.mod
            case 'harmonic'
                param.M = (param.Hshort'\param.Em)*param.Hnext'*param.Hnext;
            case 'radau-lanczos'
                s = param.svec(end);
                Imless = eye(param.restart_length-1);
                Emless = kron(Imless,eye(s)); Emless = Emless(:,end-s+1:end);
                Dvec = (param.Hshort(1:end-s,1:end-s) - kron(Imless,param.Theta))\Emless;
                Hless = param.Hshort(end-s+1:end,end-2*s+1:end-s);
                param.M = param.Em*(param.Theta + Hless*Dvec(end-s+1:end,:)'*Hless'...
                    - param.Hshort(end-s+1:end,end-s+1:end));
            case 'radau-arnoldi'
                Im = eye(param.restart_length);
                Gamma = -param.Hnext*param.Em'*((param.Hshort - kron(Im,param.Theta))\param.Em);
                param.M = (param.Em/Gamma)*param.Hnext;
            case 'none'                                                         % mainly for debugging purposes
                param.M = 0*param.Em;
        end
    end
    Xim0 = ((param.Hshort + param.M*param.Em')\param.E1)*param.Beta*Xim0(end-s+1:end,:);
    err_apx = param.Vshort*Xim0;
    param.approx_error(k-1) = block_norm(err_apx,param,A);                  % store approximate error from previous cycle

    T(k) = T(k) + toc;
    switch param.conv_check
        case 'exact'
            err_check = param.exact_error(k-1);
            if k > 2
                err_check_past = param.exact_error(1);
            end
        case 'approx'
            err_check = param.approx_error(k-1);
            if k > 2
                err_check_past = param.approx_error(1);
            end
    end
    if param.verbose
        fprintf('     %s: cycle %g, %s error = %1.4g\n',...
            mfilename,k-1,param.conv_check,err_check);
    end
    tic;

    if err_check < param.tol_err                                            % check for convergence
        param.flag = 1;
        break
    elseif k > 2 && param.halt_if_diverge                                     % check for stagnation
        if err_check >= err_check_past

            T(k) = T(k) + toc;
            if param.verbose
                fprintf('     %s: stagnation at cycle %g\n',mfilename,k-1);
            end
            tic;

            param.flag = 2;                                                 % stagnation
            break
        end
    end
    param.Fm = param.Fm + err_apx;                                          % update solution

    T(k) = T(k) + toc;
    if exact_flag
        param.exact_error(k) = block_norm(param.Fm - param.exact,param,A)/...
            param.error_scale;
    end
    tic;
end
if param.max_restarts == 1                                                  % when max_restarts = 1
    k = 1;
end
param.Acount = Acount/k;

T(k) = T(k) + toc;
param.time = T;

if k == param.max_restarts && param.flag == 0
    param.flag = 3;                                                         % maximum number of cycles reached
end

print_flag(param.flag);
Fm = param.Fm;
end

function print_flag(flag)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('Only one cycle, huh?\n')
        else
            error('Flag unchanged.  Check code for bugs.\n')
        end
    case 1
        fprintf('Method converged to desired tolerance.\n')
    case 2
        fprintf('Method stagnated before reaching desired tolerance.\n')
    case 3
        fprintf('Maximum number of cycles reached before convergence.\n')
end
end
