function [w, t] = quad_userf(N,param)
% [w, t] = QUAD_USERF(N,param) computes the N weights and nodes
% corresponding to the user-defined matrix function userf.  Weights w and
% nodes t must satisfy the form of the approximation to the matrix error
% function given as
%
% \sum_{j=1}^N w_j * V_m * (H_m + t_j*I)\ E_1 C(t_j),
%
% where C represents the accumulated product of cospatial factors.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
w = [];
t = [];
end
