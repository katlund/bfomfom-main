function param = lanczos(A,b,param)
% param = LANCZOS(A,b,param) computes the Lanczos basis with respect to A
% and b.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
m = param.restart_length;
V = [];
H = [];
if param.first_cycle || ~isfield(param,'first_cycle')
    beta = norm(b);
    V(:,1) = b/beta;
    param.beta = beta;
else
    V(:,1) = b;
end

param.Acount = 0;
for j = 1:m
    if j == 1
        if isnumeric(A)
            W = A*V(:,1);
        else
            W = A(V(:,1));
        end
    else
        if isnumeric(A)
            W = A*V(:,j) - V(:,j-1)*H(j,j-1);
        else
            W = A(V(:,j)) - V(:,j-1)*H(j,j-1);
        end
    end
    param.Acount = param.Acount + 1;
    H(j,j) = V(:,j)'*W;
    W = W - V(:,j)*H(j,j);
    H(j+1,j) = norm(W);
    if abs(H(j+1,j)) <= param.tol_zero
        m = j;
        param.Hnext = H(end,end);
        param.Hshort = H(1:m,1:m);
        param.Vnext = V(:,end);
        param.Vshort = V(:,1:end-1);
        param.breakdown = j;
        break
    end
    if j < m
        H(j,j+1) = H(j+1,j)';
    end
    V(:,j+1) = W/H(j+1,j);
end

e1 = zeros(m,1); em = e1;
e1(1) = 1; em(end) = 1;
param.e1 = e1;
param.em = em;

param.Hnext = H(end,end);
param.Hshort = H(1:m,1:m);
if flag
    param.Vnext = [];
    param.Vshort = V;
else
    param.Vnext = V(:,end);
    param.Vshort = V(:,1:end-1);
    param.breakdown = m;
end
end
