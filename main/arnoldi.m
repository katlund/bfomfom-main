function param = arnoldi(A,b,param)
% param = ARNOLDI(A,b,param) computes the Arnoldi basis with respect to A
% and b.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
m = param.restart_length;
V = [];
H = [];
if param.first_cycle || ~isfield(param,'first_cycle')
    beta = norm(b);
    V(:,1) = b/beta;
    param.beta = beta;
else
    V(:,1) = b;
end

param.Acount = 0;
flag = false;
for j = 1:m
    if isnumeric(A)
        W = A*V(:,j);
    else
        W = A(V(:,j));
    end
    param.Acount = param.Acount + 1;
    for k = 1:j
        H(k,j) = V(:,k)'*W;
        W = W - V(:,k)*H(k,j);
    end
    H(j+1,j) = norm(W);
    if abs(H(j+1,j)) <= param.tol_zero
        m = j;
        param.Hnext = H(end,end);
        param.Hshort = H(1:m,1:m);
        param.Vnext = V(:,end);
        param.Vshort = V(:,1:end-1);
        param.breakdown = j;
        e1 = zeros(m,1); e1(1) = 1;
        param.e1 = e1;
        flag = true;
        break
    end
    V(:,j+1) = W/H(j+1,j);
end

e1 = zeros(m,1); em = e1;
e1(1) = 1; em(end) = 1;
param.e1 = e1;
param.em = em;

param.Hnext = H(end,end);
param.Hshort = H(1:m,1:m);
if flag
    param.Vnext = [];
    param.Vshort = V;
else
    param.Vnext = V(:,end);
    param.Vshort = V(:,1:end-1);
    param.breakdown = m;
end
end
