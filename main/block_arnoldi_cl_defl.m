function param = block_arnoldi_cl_defl(A,B,param)
% param = BLOCK_ARNOLDI_CL_DEFL(A,B,param) computes the block Arnoldi basis
% with respect to A, B, and the classical inner product with deflation,
% similar to what is performed in BLOCK_LANCZOS_CL_DEFL.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
m = param.restart_length;
if param.first_cycle
    [Q, R, p] = qr(B,0);
    I = speye(length(p));
    Pi = I(:,p);

    absdiagR = abs(diag(R));
    ind_inex_dfl = absdiagR > param.tol_defl;                               % deflation check
    svec = sum(ind_inex_dfl);                                               % store block size
    V = Q(:,ind_inex_dfl);                                                  % store new linearly independent basis vectors

    ind_ex_dfl = absdiagR <= param.tol_zero;                                % indices with exact deflation
    Vkeep = B*Pi; Vkeep = Vkeep(:,~ind_inex_dfl & ~ind_ex_dfl);             % keep inexactly delfated vectors
    R = R/Pi;

    param.Beta = R(ind_inex_dfl,:);
else
    V = B;
    [n, svec] = size(B);
    Vkeep = zeros(n,0);
end
H = [];


jnew = 1:svec;
param.Acount = 0;
for j = 1:m
    if isnumeric(A)
        W = A*V(:,jnew);
    else
        W = A(V(:,jnew));
    end
    param.Acount = param.Acount + 1;
    for jj = 1:j
        jold = (jj-1)*svec(end)+1:jj*svec(end);
        H(jold,jnew) = V(:,jold)'*W;
        W = W - V(:,jold)*H(jold,jnew);
    end
    W = W - Vkeep*(Vkeep'*W);
    [Q, R, p] = qr(W,0);
    I = speye(length(p));
    Pi = I(:,p);

    absdiagR = abs(diag(R));
    ind_inex_dfl = absdiagR > param.tol_defl;                               % deflation check
    svec = [svec sum(ind_inex_dfl)];                                        % store new block size
    if svec(end) == 0
        param.breakdown = j;
        break
    else
        V = [V Q(:,ind_inex_dfl)];                                          % store new Cl basis vectors

        ind_ex_dfl = absdiagR <= param.tol_zero;                            % indices with exact deflation
        Y = V(:,jnew)*Pi; Y = Y(:,~ind_inex_dfl & ~ind_ex_dfl);             % figure out deflated vectors
        Vkeep = [Vkeep Y];                                                  % keep deflated vectors

        jold = jnew;                                                        % update indices
        jnew = jold(end)+1:jold(end)+svec(end);

        R = R/Pi;
        H(jnew,jold) = R(ind_inex_dfl,:);
        if j < m
            H(jold,jnew) = H(jnew,jold)';
        end
    end
end

Is = eye(svec(1));
E1 = zeros(size(H,2),svec(1)); E1(1:svec(1),1:svec(1)) = Is;
param.E1 = E1;

Em = zeros(size(H,2),svec(end-1));
Em(end-svec(end-1)+1:end,end-svec(end-1)+1:end) = eye(svec(end-1));
param.Em = Em;

param.svec = svec;
param.Vkeep = Vkeep;
param.Hnext = H(end-svec(end)+1:end,end-svec(end-1)+1:end);
param.Hshort = H(1:end-svec(end),:);
param.Hfull = H;
param.Vnext = V(:,end-svec(end)+1:end);
param.Vshort = V(:,1:end-svec(end));
param.breakdown = m;
end
