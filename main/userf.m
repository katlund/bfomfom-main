function out = userf(H)
% out = USERF(H) is a a template for a matrix function custom-defined by
% the user for (b)fomfom(_mod).
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
% % One can use a built-in function:
% out = sqrtm(H);

% % Or write a routine directly into this file:
% out = H^2;
% out = -out\eye(size(out));

% % Or out-source some other process:
% out = another_function(H);

end
