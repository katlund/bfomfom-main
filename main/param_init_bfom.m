function [param, modified] = param_init_bfom(param)
% [param,modified] = PARAM_INIT_BFOM(param) generates and/or checks the
% input parameter struct.
%
% param = PARAM_INIT_BFOM returns a default setting
% param = PARAM_INIT_BFOM(param) returns a corrected parameter setting
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%

modified = 0;

if ~nargin
    param = struct;
    modified = 1;
end

if ~isfield(param,'verbose')
    param.verbose = 1;
    disp('Warning: .verbose not specified, set to 1.');
    modified = 1;
end

if ~isfield(param,'exact')
    param.conv_check = 'approx';
    if param.verbose
        disp('Warning: .exact not specified, no error available and .conv_check set to ''approx'', which may be inaccurate.');
    end
    modified = 1;
else
    if ~isfield(param,'conv_check')
        param.conv_check = 'exact';
        if param.verbose
            disp('Warning: .conv_check not specified, set to ''exact''.');
        end
        modified = 1;
    end
end

if ~isfield(param,'error_scale') && isfield(param,'exact')
    param.error_scale = norm(param.exact,'fro');
end

if ~isfield(param,'function')
    param.function = 'inv';
end

if ~isfield(param,'hermitian')
    param.hermitian = false;
    if param.verbose
        disp('Warning: .hermitian not specified, set to false.');
    end
    modified = 1;
end

if ~isfield(param,'inner_product')
    param.inner_product = 'gl';
    if param.verbose
        disp('Warning: .inner_product not specified, set to ''gl''.');
    end
    modified = 1;
else
    if strcmp(param.inner_product,'cl_defl')
        if ~isfield(param,'tol_defl')
            param.tol_defl = 1e-6;
            if param.verbose
                disp('Warning: .tol_defl not specified, set to 1e-6.');
            end
            modified = 1;
        end
    end
end

if ~isfield(param,'max_restarts')
    param.max_restarts = 100;
    if param.verbose
        disp('Warning: .max_restarts not specified, set to 100.');
    end
    modified = 1;
end

if ~isfield(param,'halt_if_diverge')
    param.halt_if_diverge = 1;
    if param.verbose
        disp('Warning: .halt_if_diverge not specified, set to 1.');
    end
    modified = 1;
end

if ~isfield(param,'norm')
    if isfield(param,'hermitian')
        if param.hermitian
            param.norm = 'A-fro';
            if param.verbose
                disp('Warning: .norm not specified, set to ''A-fro''.');
            end
        else
            param.norm = 'fro';
            if param.verbose
                disp('Warning: .norm not specified, set to ''fro''.');
            end
        end
    else
        param.norm = 'fro';
        if param.verbose
            disp('Warning: .norm not specified, set to ''fro''.');
        end
    end

end

if ~isfield(param,'restart_length')
    param.restart_length = 25;
    if param.verbose
        disp('Warning: .restart_length not specified, set to 25.');
    end
    modified = 1;
end

if ~isfield(param,'tol_err')
    param.tol_err = 1e-6;
    if param.verbose
        disp('Warning: .tol_err not specified, set to 1e-6.');
    end
    modified = 1;
end

if ~isfield(param,'tol_zero')
    param.tol_zero = 1e-13;
    if param.verbose
        disp('Warning: .tol_zero not specified, set to 1e-13.');
    end
    modified = 1;
end

end
