function out = krylov_basis_eval(param,X)
% out = KRYLOV_BASIS_EVAL(param,X) evaluates the Krylov basis specified by
% param on a block vector X efficiently according to the inner product
% specified by param.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
switch param.inner_product
    case {'cl_full', 'li', 'hy'}
        out = param.Vshort*X;
    case 'gl'
        [n, m1s] = size(param.Vshort);
        [m1, m2] = size(X);
        s = m1s/m1;
        out = zeros(n,m2*s);
        for i = 1:m1  % run through block vectors of the basis
            indi = (i-1)*s+1:i*s;
            for j = 1:m2 % run through the block vectors of the output
                indj = (j-1)*s+1:j*s;
                out(:,indj) = out(:,indj) + param.Vshort(:,indi)*X(i,j);
            end
        end
end
end
