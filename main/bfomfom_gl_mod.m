function param = bfomfom_gl_mod(A,B,param)
% param = BFOMFOM_GL_MOD(A,B,param) computes the modified restarted global
% B(FOM)^2 approximation to the matrix function product f(A)B.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
fprintf('%s\n',mfilename)
global delta;
global T; T = 0;                                                            % pre-set walltime
tic;
param.first_cycle = true;                                                   % store Beta
param = krylov_basis(A,B,param);
Acount = param.Acount;

% Extract quantities needed for cospatial function. We have to break
% dependence on param; otherwise the in-line functions will store all
% previous param, which includes all previous basis vectors.
beta = param.beta;
e1 = param.e1;
em = param.em;
Hnext = param.Hnext;
Hshort = param.Hshort;

s = size(B,2);
if ~isfield(param,'mod')
    M = 0*em;
else
    switch param.mod
        case 'harmonic'
            M = (Hshort'\em)*(Hnext'*Hnext);
        case 'radau-lanczos'
            param.theta = max(diag(param.Theta));
            Imless = eye(param.breakdown-1);
            emless = Imless(:,end);
            dvec = (Hshort(1:end-1,1:end-1) - Imless*param.theta)\emless;
            M = em*(param.theta + ...
                Hshort(end,end-1)*dvec(end)'*Hshort(end,end-1)'+...
                - Hshort(end,end));
        case 'radau-arnoldi'
            param.theta = max(diag(param.Theta));
            Im = eye(param.restart_length);
            Gamma = -Hnext*em'*((Hshort - Im*param.theta)\em);
            M = (em/Gamma)*Hnext;
        case 'none'
            M = 0*em;
    end
end
Xim = @(t) ((Hshort + t*eye(size(Hshort)) + M*em')\e1)*beta;
if strcmp(param.function,'exp')
    param.ritz = eig(Hshort + M*em');
end

global Is; Is = eye(s);
switch param.function                                                       % compute initial approximation
    case 'invSqrt'
        delta = 1;
        param.alpha = .5;
        F = sqrtm(Hshort + M*em')\e1;
        param.quad = @(N,param) quad_invAlpha(N,.5);

    case 'invAlpha'
        delta = 1;
        F = (Hshort + M*em')^param.alpha\e1;
        param.quad = @(N,param) quad_invAlpha(N,param.alpha);

    case 'logShiftInv'
        F = logm(Hshort + M*em' + eye(size(Hshort)))*((Hshort + M)\e1);
        param.quad = @(N,param) quad_logShiftInv(N);

    case 'exp'
        F = expm(Hshort + M*em')*e1;
        param.quad = @(N,param) quad_exp(N,param);

    case 'user'
        F = userf(Hshort + M*em')*e1;
        param.quad = @(N,param) quad_userf(N,param);
end
param.Fm = 0;
Vstart = -param.Vnext*Hnext;
ind = 1:s;
for i = 1:param.breakdown
    param.Fm = param.Fm + param.Vshort(:,ind)*F(i);
    Vstart = Vstart + param.Vshort(:,ind)*M(i);
    ind = ind + s;
end
param.Fm = param.Fm*beta;

T = T + toc;
if isfield(param,'exact')
    exact_flag = true;
    param.exact_error = block_norm(param.Fm - param.exact,param,A)/...
        param.error_scale;                                                  % compute exact error
    if param.exact_error < param.tol_err
        param.flag = 1;
        print_flag(param.flag);
        return
    end
else
    exact_flag = false;
end

param.Nquad_per_cycle = [];                                                 % pre-set storage for quadrature node count
param.approx_error = [];                                                    % pre-set storage for approximate error
param.flag = 0;                                                             % initialize convergence flag
for k = 2:param.max_restarts
    tic; T(k) = 0;
    Gm = @(t) em'*Xim(t);
    param = krylov_basis(A,Vstart,param);
    Acount = Acount + param.Acount;

    % Extract quantities needed for cospatial function update
    beta = param.beta;
    e1 = param.e1;
    em = param.em;
    Hnext = param.Hnext;
    Hshort = param.Hshort;

    if ~isfield(param,'mod')
        M = 0*em;
    else
        switch param.mod
            case 'harmonic'
                M = (Hshort'\em)*(Hnext'*Hnext);
            case 'radau-lanczos'
                param.theta = max(diag(param.Theta));
                Imless = eye(param.breakdown-1);
                emless = Imless(:,end);
                dvec = (Hshort(1:end-1,1:end-1) - Imless*param.theta)\emless;
                M = em*(param.theta + ...
                    Hshort(end,end-1)*dvec(end)'*Hshort(end,end-1)'+...
                    - Hshort(end,end));
            case 'radau-arnoldi'
                param.theta = max(diag(param.Theta));
                Im = eye(param.restart_length);
                Gamma = -Hnext*em'*((Hshort - Im*param.theta)\em);
                M = (em/Gamma)*Hnext;
            case 'none'
                M = 0*em;
        end
    end
    Xim = @(t) ((Hshort + t*eye(size(Hshort)) + M*em')\e1)*beta*Gm(t);
    if strcmp(param.function,'exp')
        param.ritz = [param.ritz; eig(Hshort + M*em')];
    end

    % error function
    param = mat_err_func_mod(param,Xim,k);
    if param.flag == 4
        param.Nquad_per_cycle(k-1) = param.Nquadmax;
        break
    end
    err_apx = 0;
    Vstart = -param.Vnext*Hnext;
    ind = 1:s;
    for i = 1:param.breakdown
        err_apx = err_apx + param.Vshort(:,ind)*param.Delta(i);
        Vstart = Vstart + param.Vshort(:,ind)*M(i);
        ind = ind + s;
    end
    param.Nquad_per_cycle(k-1) = param.Nquad2;

    param.approx_error(k-1) = block_norm(err_apx,param,A)/...
        param.error_scale;                                                  % store approximate error from previous cycle

    T(k) = T(k) + toc;
    switch param.conv_check
        case 'exact'
            err_check = param.exact_error(k-1);
            if k > 2
                err_check_past = param.exact_error(1);
            end
        case 'approx'
            err_check = param.approx_error(k-1);
            if k > 2
                err_check_past = param.approx_error(1);
            end
    end
    if param.verbose
        fprintf('     %s: cycle %g, %s error = %1.4g\n',...
            mfilename,k-1,param.conv_check,err_check);
    end
    tic;

    if err_check < param.tol_err                                            % check for convergence
        param.flag = 1;
        break
    elseif k > 2 && param.halt_if_diverge                                     % check for stagnation
        if err_check >= err_check_past
            T(k) = T(k) + toc;
            if param.verbose
                fprintf('     %s: stagnation at cycle %g\n',mfilename,k-1);
            end
            tic;

            param.flag = 2;                                                 % stagnation
            break
        end
    end
    param.Fm = param.Fm + err_apx;                                          % update solution approximation

    T(k) = T(k) + toc;
    if exact_flag
        param.exact_error(k) = block_norm(param.Fm - param.exact,param,A)/...
            param.error_scale;
    end
    tic;

    if param.breakdown < param.restart_length
        param.flag = 5;
        break
    end

    if param.reduce_Nquad                                                   % reduce number of quadrature nodes
        if ~param.refined && param.Nquad1 > 2
            param.Nquad2 = param.Nquad1;
            param.Nquad1 = round(param.Nquad1/sqrt(2));
            if mod(param.Nquad1,2) == 1
                param.Nquad1 = param.Nquad1-1;
            end
        end
    end
end
if param.max_restarts == 1                                                  % when max_restarts = 1
    k = 1;
end
param.Acount = Acount/k;

T(k) = T(k) + toc;
param.time = T;

if k == param.max_restarts && param.flag == 0
    param.flag = 3;                                                         % maximum number of cycles reached
end

print_flag(param.flag)
end

%% Auxiliary functions

function print_flag(flag)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('Only one cycle, huh?\n')
        else
            error('Flag unchanged.  Check code for bugs.\n')
        end
    case 1
        fprintf('Method converged to desired tolerance.\n')
    case 2
        fprintf('Method stagnated before reaching desired tolerance.\n')
    case 3
        fprintf('Maximum number of cycles reached before convergence.\n')
    case 4
        fprintf('Maximum number of quadrature nodes reached before convergence.\n')
    case 5
        fprintf('Method broke down-- check error to see if desired tolerance reached.\n')
end
end
