function param = block_arnoldi_li(A,B,param)
% param = BLOCK_ARNOLDI_LI(A,B,param) computes the block Arnoldi basis with
% respect to A, B, and the loop-interchange inner product without
% short-cuts.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
m = param.restart_length;
if param.first_cycle
    [V, param.Beta] = N(B);
else
    V = B;
end
H = [];

s = size(B,2);
jnew = 1:s;
param.Acount = 0;
for j = 1:m
    if isnumeric(A)
        W = A*V(:,jnew);
    else
        W = A(V(:,jnew));
    end
    param.Acount = param.Acount + 1;
    for jj = 1:j
        jold = (jj-1)*s+1:jj*s;
        H(jold,jnew) = IP(V(:,jold),W);
        W = W - V(:,jold)*H(jold,jnew);
    end
    jold = jnew;
    jnew = jold(end)+1:jold(end)+s;
    [Q, H(jnew,jold)] = N(W);
    V = [V Q];                                                              % store new basis vectors
end

Is = eye(s);
E1 = zeros(size(H,2),s); E1(1:s,1:s) = Is;
param.E1 = E1;

Em = zeros(size(H,2),s);
Em(end-s+1:end,end-s+1:end) = Is;
param.Em = Em;

param.svec = s*ones(1,m+1);
param.Hnext = H(end-s+1:end,end-s+1:end);
param.Hshort = H(1:end-s,:);
param.Hfull = H;
param.Vnext = V(:,end-s+1:end);
param.Vshort = V(:,1:end-s);
param.breakdown = m;
end

function S = IP(X,Y)
S = diag(diag(X'*Y));
end

function [Q, R] = N(X)
R = diag(sqrt(diag(X'*X)));
Q = X/R;
end
