function AA = fold(A,n,s,p)
% AA = FOLD(A,n,s,p) takes an np x s block vector A and returns an n x s x
% p tensor AA such that the n x s block entries of A are the frontal faces
% of AA.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%
% This file was written specifically for [Lund2020].

%%
% Assumes that A is an np x s matrix
AA = zeros(n,s,p);
ind = 1:n;
for k = 1:p
    AA(:,:,k) = A(ind,:);
    ind = ind + n;
end
end
