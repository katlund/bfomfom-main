function P = matpolyval(C,A,V)
% P = MATPOLYVAL(C,A,V) evaluates P(A) \circ V, where C is a vector of
% block coefficients for the matrix-valued polynomial P. C, V, and A must
% be of compatible dimensions, i.e., C is s x s, V is n x s, and A is n x
% n; alternatively, V could be scalar, and A scalar or s x s.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%%
[ns, s] = size(C);
n = ns/s;
if isscalar(V)
    ind = s*(n-1)+1:s*n;
    P = C(ind,:);
    for j = n-1:-1:1
        ind = ind-s;
        P = C(ind,:) + A*P;
    end
    P = P*V;
else                                   % then treat like a matrix
    ind = s*(n-1)+1:s*n;
    P = V*C(ind,:);
    for j = n-1:-1:1
        ind = ind-s;
        P = V*C(ind,:) + A*P;
    end
end

end
