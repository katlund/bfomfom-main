function param = bfom(A,B,param)
% param = BFOM(A,B,param) is a wrapper function for the variations of BFOM.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
if isfield(param,'mod')
    switch param.inner_product
        case {'cl_defl', 'cl_full', 'li', 'hy', 'ud'}
            param = bfom_cl_mod(A,B,param);
        case 'gl'
            param = bfom_gl_mod(A,B,param);
        case 'nb'
            param = fom_mod(A,B,param);
    end
else
    switch param.inner_product
        case {'cl_defl', 'cl_full', 'li', 'hy', 'ud'}
            param = bfom_cl(A,B,param);
        case 'gl'
            param = bfom_gl(A,B,param);
        case 'nb'
            param = fom(A,B,param);
    end
end
end
