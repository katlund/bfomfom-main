function A = unfold(AA,n,s,p)
% A = UNFOLD(AA,n,s,p) takes a third-order tensor AA of dimensions n, s,
% and p and returns an np x s block vector A, whose n x s block entries are
% the p frontal faces of AA.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].
%
% This file was written specifically for [Lund2020].

%%
% Assumes that AA is an n x s x p array
A = zeros(n*p,s);
ind = 1:n;
for k = 1:p
    A(ind,:) = AA(:,:,k);
    ind = ind + n;
end
end
