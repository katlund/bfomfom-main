function param = fom(A,B,param)
% param = FOM(A,B,param) computes the restarted FOM approximation to the
% linear system AX = B by solving the s systems Ax_i = b_i, i = 1,...,s, in
% serial.
%
% This file is a part of the B(FOM)^2 code described in detail in
% [FrommerLundSzyld2017] and [FrommerLundSzyld2020].

%%
fprintf('%s\n',mfilename)
s = size(B,2);                                                              % determine number of right-hand sides
param.tol_err = param.tol_err/s;                                            % since we check error column by column
param.Fm = B;                                                               % pre-set storage for Fm

T = cell(1,s);                                                              % pre-set time
err_vec = cell(1,s);                                                        % pre-set error
err_apx_vec = cell(1,s);
Acount = cell(1,s);

kvec = param.max_restarts*ones(1,s);                                        % index storage for constructing Frobenius norm at the end
param.flag = zeros(1,s);                                                    % initialize convergence flag

for i = 1:s
    tic; T{i} = 0;
    param.first_cycle = true;                                               % store beta
    param = krylov_basis(A,B(:,i),param);
    Acount{i}(1) = param.Acount;
    Xi = param.Hshort\param.e1;
    param.Fm(:,i) = param.Vshort*Xi*param.beta;
    param.Cm = -param.Hnext*Xi(end)*param.beta;                             % colinear factors
    param.first_cycle = false;                                              % for subsequent cycles, do not recompute beta

    T{i} = T{i} + toc;
    switch param.conv_check
        case 'exact'
            exact_flag = true;
            err_vec{i}(1) = block_norm(param.Fm(:,i) - param.exact(:,i),...
                param,A);                                                   % compute exact error
            if err_vec{i}(1) < param.tol_err
                param.flag(i) = 1;
                err_apx_vec{i}(1) = NaN;
                kvec(i) = 1;
            end
        case 'approx'
            exact_flag = false;
            err_vec{i}(1) = NaN;
    end
    for k = 2:param.max_restarts
        if isempty(k)                                                       % when max_restarts = 1
            k = 1;
            break
        end
        tic; T{i}(k) = 0;
        param = krylov_basis(A,param.Vnext,param);
        Acount{i}(k) = param.Acount;

        Xi = param.Hshort\param.e1;                                          % compute approximate error from previous cycle
        Delta = Xi*param.Cm;
        err_apx = param.Vshort*Delta;
        err_apx_vec{i}(k-1) = block_norm(err_apx,param,A);                  % store approximate error from previous cycle

        T{i}(k) = T{i}(k) + toc;
        switch param.conv_check
            case 'exact'
                err_check = err_vec{i}(k-1)/param.error_scale;
                if k > 2
                    err_check_past = err_vec{i}(1)/param.error_scale;
                end
            case 'approx'
                err_check = err_apx_vec{i}(k-1)/param.error_scale;
                if k > 2
                    err_check_past = err_apx_vec{i}(1)/param.error_scale;
                end
        end
        if param.verbose
            fprintf('     %s: cycle %g, column %g, %s error = %1.4g\n',...
            mfilename,k-1,i,param.conv_check,err_check);
        end
        tic;

        if err_check <= param.tol_err                                       % check for convergence
            kvec(i) = k-1;                                                  % used for constructing Frobenius norm at the end
            param.flag(i) = 1;
            break
        elseif k > 2 && param.halt_if_diverge                                 % check for stagnation
            if err_check >= err_check_past

                T{i}(k) = T{i}(k) + toc;
                if param.verbose
                    fprintf('     %s: stagnation at cycle %g, column %g\n',...
                        mfilename,k-1, i);
                end
                tic;

                kvec(i) = k-1;                                              % used for constructing Frobenius norm at the end
                param.flag(i) = 2;                                          % stagnation
                break
            end
        end
        param.Fm(:,i) = param.Fm(:,i) + err_apx;                            % update solution approximation

        T{i}(k) = T{i}(k) + toc;
        if exact_flag
            err_vec{i}(k) = block_norm(param.Fm(:,i) - param.exact(:,i),param,A);    % compute exact error
        else
            err_vec{i}(k) = NaN;
        end
        tic;

        if param.breakdown < param.restart_length
            kvec(i) = k-1;
            param.flag(i) = 5;
            break
        else
            param.Cm = -param.Hnext*Xi(end)*param.Cm;                        % next cospatial factor
        end
    end
    T{i}(k) = T{i}(k) + toc;

    if k == param.max_restarts && param.flag(i) == 0
        param.flag(i) = 3;                                                  % maximum number of cycles reached
    end
    print_flag(param.flag(i),i);
end
max_restarts = max(kvec);

%% Compute Frobenius norm of error and average number of quadrature nodes per cycle
param.exact_error = zeros(1,max_restarts);
param.approx_error = zeros(1,max_restarts);
param.time = zeros(1,max_restarts);
param.Acount = zeros(1,max_restarts);
for j = 1:max_restarts
    for i = 1:s
        if param.flag(i) ~= 3                                               % if maximum number of cycles reached before convergence
            if length(err_vec{i}) < j                                       % if column i converged before the others, repeat last result for column i
                param.exact_error(j) = param.exact_error(j) +...
                    err_vec{i}(end)^2;
                param.approx_error(j) = param.approx_error(j) +...
                    err_apx_vec{i}(end)^2;
                param.Acount(j) = param.Acount(j) + Acount{i}(end);
            else
                param.exact_error(j) = param.exact_error(j) +...
                    err_vec{i}(j)^2;
                param.approx_error(j) = param.approx_error(j) +...
                    err_apx_vec{i}(j)^2;
                param.Acount(j) = param.Acount(j) + Acount{i}(j);
                param.time(j) = param.time(j) + T{i}(j);
            end
        else                                                                % in the case of that max_restarts was reached, err_apx_vec would be short 1 entry
            if length(err_vec{i}) < j+1                                     % if column i converged before the others, repeat last result for column i
                param.exact_error(j) = param.exact_error(j) +...
                    err_vec{i}(end)^2;
                param.approx_error(j) = param.approx_error(j) +...
                    err_apx_vec{i}(end)^2;
                param.Acount(j) = param.Acount(j) + Acount{i}(end);
            else
                param.exact_error(j) = param.exact_error(j) +...
                    err_vec{i}(j)^2;
                param.approx_error(j) = param.approx_error(j) +...
                    err_apx_vec{i}(j)^2;
                param.Acount(j) = param.Acount(j) + Acount{i}(j);
                param.time(j) = param.time(j) + T{i}(j);
            end
        end
    end
end

% Adjust outputs in order to accurately compare with block methods in
% Frobenius norm
param.tol_err = param.tol_err*s;                                            % undo initial adjustment
if exact_flag
    param.exact_error = sqrt(param.exact_error)/param.error_scale;
    ind = find(param.exact_error <= param.tol_err,1,'first');
else
    param.approx_error = sqrt(param.approx_error)/param.error_scale;
    ind = find(param.approx_error <= param.tol_err,1,'first');
end
param.exact_error(ind+1:end) = [];
param.approx_error(ind+1:end) = [];
param.time(ind+1:end) = [];
param.Acount(ind+1:end) = [];
param.Acount = round(mean(param.Acount));
end

function print_flag(flag,i)
switch flag
    case 0
        if param.max_restarts == 1
            fprintf('column %g: Only one cycle, huh?\n',i)
        else
            error('column %g: Flag unchanged.  Check code for bugs.\n',i)
        end
    case 1
        fprintf('column %g: Method converged to desired tolerance.\n',i)
    case 2
        fprintf('column %g: Method stagnated before reaching desired tolerance.\n',i)
    case 3
        fprintf('column %g: Maximum number of cycles reached before convergence.\n',i)
    case 5
        fprintf('column %g: Method broke down-- check error to see if desired tolerance reached.\n',i)
end
end
